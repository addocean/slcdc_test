    --user_data
    insert into user_data (user_id, username, password)
     values
      (1, 'container', '$2a$10$KK.D/hULDl/b5cUT/xe4RuVA2O17/r5dDgtkzcIzrdpaCruOF8ZVq');

    insert into user_data (user_id, username, password)
     values
      (2, 'vessel', '$2a$10$KK.D/hULDl/b5cUT/xe4RuVA2O17/r5dDgtkzcIzrdpaCruOF8ZVq');

    insert into user_data (user_id, username, password)
     values
      (3, 'terminal', '$2a$10$KK.D/hULDl/b5cUT/xe4RuVA2O17/r5dDgtkzcIzrdpaCruOF8ZVq');

    insert into user_data (user_id, username, password)
     values
      (4, 'aduana', '$2a$10$KK.D/hULDl/b5cUT/xe4RuVA2O17/r5dDgtkzcIzrdpaCruOF8ZVq');

    insert into user_data (user_id, username, password)
     values
      (5, 'ttp', '$2a$10$KK.D/hULDl/b5cUT/xe4RuVA2O17/r5dDgtkzcIzrdpaCruOF8ZVq');

    insert into user_data (user_id, username, password)
     values
      (6, 'apba', '$2a$10$KK.D/hULDl/b5cUT/xe4RuVA2O17/r5dDgtkzcIzrdpaCruOF8ZVq');

    insert into user_data (user_id, username, password)
     values
      (7, 'repAduanero', '$2a$10$KK.D/hULDl/b5cUT/xe4RuVA2O17/r5dDgtkzcIzrdpaCruOF8ZVq');

    insert into user_data (user_id, username, password)
     values
      (8, 'emisor', '$2a$10$KK.D/hULDl/b5cUT/xe4RuVA2O17/r5dDgtkzcIzrdpaCruOF8ZVq');

    insert into user_data (user_id, username, password)
     values
      (9, 'receptor', '$2a$10$KK.D/hULDl/b5cUT/xe4RuVA2O17/r5dDgtkzcIzrdpaCruOF8ZVq');

    insert into user_data (user_id, username, password)
     values
      (10, 'container2', '$2a$10$KK.D/hULDl/b5cUT/xe4RuVA2O17/r5dDgtkzcIzrdpaCruOF8ZVq');

    insert into user_data (user_id, username, password)
     values
      (11, 'container3', '$2a$10$KK.D/hULDl/b5cUT/xe4RuVA2O17/r5dDgtkzcIzrdpaCruOF8ZVq');

    --roles
    insert into role (role_id, role)
     values
      (1, 0);
    insert into role (role_id, role)
     values
      (2, 1);
    insert into role (role_id, role)
     values
      (3, 2);
    insert into role (role_id, role)
     values
      (4, 3);
    insert into role (role_id, role)
     values
      (5, 4);
    insert into role (role_id, role)
     values
      (6, 5);
    insert into role (role_id, role)
     values
      (7, 6);
    insert into role (role_id, role)
     values
      (8, 7);
    insert into role (role_id, role)
     values
      (9, 8);


    --users_role_detail
    insert into users_role_detail (user_data_user_id, role_id_role_id)
     values
      ((select user_id from user_data where username = 'container'), (select role_id from role where role = 0));

    insert into users_role_detail (user_data_user_id, role_id_role_id)
     values
      ((select user_id from user_data where username = 'container2'), (select role_id from role where role = 0));

    insert into users_role_detail (user_data_user_id, role_id_role_id)
     values
      ((select user_id from user_data where username = 'container3'), (select role_id from role where role = 0));

    insert into users_role_detail (user_data_user_id, role_id_role_id)
     values
      ((select user_id from user_data where username = 'vessel'), (select role_id from role where role = 1));

    insert into users_role_detail (user_data_user_id, role_id_role_id)
     values
      ((select user_id from user_data where username = 'terminal'), (select role_id from role where role = 2));

    insert into users_role_detail (user_data_user_id, role_id_role_id)
     values
      ((select user_id from user_data where username = 'aduana'), (select role_id from role where role = 3));

    insert into users_role_detail (user_data_user_id, role_id_role_id)
     values
      ((select user_id from user_data where username = 'ttp'), (select role_id from role where role = 4));

    insert into users_role_detail (user_data_user_id, role_id_role_id)
     values
      ((select user_id from user_data where username = 'apba'), (select role_id from role where role = 5));

    insert into users_role_detail (user_data_user_id, role_id_role_id)
     values
      ((select user_id from user_data where username = 'repAduanero'), (select role_id from role where role = 6));

    insert into users_role_detail (user_data_user_id, role_id_role_id)
     values
      ((select user_id from user_data where username = 'emisor'), (select role_id from role where role = 7));

    insert into users_role_detail (user_data_user_id, role_id_role_id)
     values
      ((select user_id from user_data where username = 'receptor'), (select role_id from role where role = 8));

    --ship
    insert into ship (ship_id, call_sign, flag, imo_code, mmsi, ship_name)
     values
      (1, 'call1', 'flag1', 'imo1', 'mmsi1', 'ship1');

    insert into ship (ship_id, call_sign, flag, imo_code, mmsi, ship_name)
     values
      (2, 'call2', 'flag2', 'imo2', 'mmsi2', 'ship2');

    insert into ship (ship_id, call_sign, flag, imo_code, mmsi, ship_name)
     values
      (3, 'call3', 'flag3', 'imo3', 'mmsi3', 'ship3');

    insert into ship (ship_id, call_sign, flag, imo_code, mmsi, ship_name)
     values
      (4, 'call4', 'flag4', 'imo4', 'mmsi4', 'ship4');

    insert into ship (ship_id, call_sign, flag, imo_code, mmsi, ship_name)
     values
      (5, 'call5', 'flag5', 'imo5', 'mmsi5', 'ship5');

    insert into ship (ship_id, call_sign, flag, imo_code, mmsi, ship_name)
     values
      (6, 'call6', 'flag6', 'imo6', 'mmsi6', 'ship6');

    insert into ship (ship_id, call_sign, flag, imo_code, mmsi, ship_name)
     values
      (7, 'call7', 'flag7', 'imo7', 'mmsi7', 'ship7');

    insert into ship (ship_id, call_sign, flag, imo_code, mmsi, ship_name)
     values
      (8, 'call8', 'flag8', 'imo8', 'mmsi8', 'ship8');

    --agent
    insert into agent (agent_id, agent_cif, agent_name)
     values
      (1, '111111111', 'agent1');

    insert into agent (agent_id, agent_cif, agent_name)
     values
      (2, '222222222', 'agent2');

    insert into agent (agent_id, agent_cif, agent_name)
     values
      (3, '333333333', 'agent3');

    insert into agent (agent_id, agent_cif, agent_name)
     values
      (4, '444444444', 'agent4');

    insert into agent (agent_id, agent_cif, agent_name)
     values
      (5, '555555555', 'agent5');

    insert into agent (agent_id, agent_cif, agent_name)
     values
      (6, '666666666', 'agent6');

    insert into agent (agent_id, agent_cif, agent_name)
     values
      (7, '777777777', 'agent7');

    --visit
    insert into visit (visit_id, visit_eta, visit_etd, visit_ata, visit_atd, next_port_code, visit_status_id, visit_number, ship_id, agent_id)
        values
            (1, '2019-02-05', '2019-02-09', '2019-02-06', '2019-02-10', 'port_code1', 'visit_status1', '201999001',
             (select ship_id from ship where imo_code = 'imo1'),
             (select agent_id from agent where agent_name = 'agent1'));

    insert into visit (visit_id, visit_eta, visit_etd, visit_ata, visit_atd, next_port_code, visit_status_id, visit_number, ship_id, agent_id)
        values
            (2, '2019-02-06', '2019-02-10', '2019-02-07', '2019-02-11', 'port_code2', 'visit_status2', '201999002',
             (select ship_id from ship where imo_code = 'imo2'),
             (select agent_id from agent where agent_name = 'agent2'));

    insert into visit (visit_id, visit_eta, visit_etd, visit_ata, visit_atd, next_port_code, visit_status_id, visit_number, ship_id, agent_id)
        values
            (3, '2019-02-07', '2019-02-11', '2019-02-07', '2019-02-12', 'port_code3', 'visit_status3', '201999003',
             (select ship_id from ship where imo_code = 'imo3'),
             (select agent_id from agent where agent_name = 'agent3'));

    insert into visit (visit_id, visit_eta, visit_etd, visit_ata, visit_atd, next_port_code, visit_status_id, visit_number, ship_id, agent_id)
        values
            (4, '2019-02-08', '2019-02-12', '2019-02-08', '2019-02-13', 'port_code4', 'visit_status4', '201999004',
             (select ship_id from ship where imo_code = 'imo4'),
             (select agent_id from agent where agent_name = 'agent4'));

    insert into visit (visit_id, visit_eta, visit_etd, visit_ata, visit_atd, next_port_code, visit_status_id, visit_number, ship_id, agent_id)
        values
            (5, '2019-02-09', '2019-02-13', '2019-02-09', '2019-02-13', 'port_code5', 'visit_status5', '201999005',
             (select ship_id from ship where imo_code = 'imo5'),
             (select agent_id from agent where agent_name = 'agent5'));

    insert into visit (visit_id, visit_eta, visit_etd, visit_ata, visit_atd, next_port_code, visit_status_id, visit_number, ship_id, agent_id)
        values
            (6, '2019-02-10', '2019-02-14', '2019-02-10', '2019-02-14', 'port_code6', 'visit_status6', '201999006',
             (select ship_id from ship where imo_code = 'imo6'),
             (select agent_id from agent where agent_name = 'agent6'));

    insert into location (location_id, location_name) values (1, 'APMT');
    insert into location (location_id, location_name) values (2, 'TTIA');

    --stop
    insert into stop (stop_id, stop_eta, stop_etd, stop_ata, stop_atd, visit_id, location_id)
        values
            (1, '2019-02-05', '2019-02-09', '2019-02-06', '2019-02-10',
            (select visit_id from visit where visit_number = '201999001'), (select location_id from location where location_name = 'APMT'));

    insert into stop (stop_id, stop_eta, stop_etd, stop_ata, stop_atd, visit_id, location_id)
        values
            (2, '2019-02-06', '2019-02-10', '2019-02-07', '2019-02-11',
            (select visit_id from visit where visit_number = '201999002'), (select location_id from location where location_name = 'APMT'));

    insert into stop (stop_id, stop_eta, stop_etd, stop_ata, stop_atd, visit_id, location_id)
        values
            (3, '2019-02-07', '2019-02-11', '2019-02-08', '2019-02-11',
            (select visit_id from visit where visit_number = '201999003'), (select location_id from location where location_name = 'APMT'));

    insert into stop (stop_id, stop_eta, stop_etd, stop_ata, stop_atd, visit_id, location_id)
        values
            (4, '2019-02-08', '2019-02-12', '2019-02-09', '2019-02-12',
            (select visit_id from visit where visit_number = '201999004'), (select location_id from location where location_name = 'APMT'));

    insert into stop (stop_id, stop_eta, stop_etd, stop_ata, stop_atd, visit_id, location_id)
        values
            (5, '2019-02-09', '2019-02-13', '2019-02-10', '2019-02-13',
            (select visit_id from visit where visit_number = '201999005'), (select location_id from location where location_name = 'TTIA'));

    insert into stop (stop_id, stop_eta, stop_etd, stop_ata, stop_atd, visit_id, location_id)
        values
            (6, '2019-02-10', '2019-02-14', '2019-02-11', '2019-02-14',
            (select visit_id from visit where visit_number = '201999006'), (select location_id from location where location_name = 'TTIA'));

    insert into list (list_id, created_user, stop_id) values (1, (select user_id from user_data where username = 'container'), 1);