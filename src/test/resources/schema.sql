drop view if exists utiView;
drop view if exists listView;
drop view if exists stopView;

drop table if exists record cascade;
drop table if exists dangerous_good cascade;
drop table if exists seal cascade;
drop table if exists uti cascade;
drop table if exists acceptlist cascade;
drop table if exists condition cascade;
drop table if exists reporttransmission cascade;
drop table if exists listtransmission cascade;
drop table if exists file cascade;
drop table if exists report cascade;
drop table if exists list cascade;
drop table if exists stop cascade;
drop table if exists visit cascade;
drop table if exists refeer cascade;
drop table if exists location cascade;
drop table if exists agent cascade;
drop table if exists ship cascade;
drop table if exists users_role_detail cascade;
drop table if exists role cascade;
drop table if exists user_data cascade;
drop table if exists acceptlist cascade;

-- SEQUENCE: public.location_location_id_seq

DROP SEQUENCE if exists public.location_location_id_seq;

CREATE SEQUENCE public.location_location_id_seq
    INCREMENT 1
    START 5
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.location_location_id_seq
    OWNER TO docker;

-- SEQUENCE: public.listtransmission_transmission_id_seq

DROP SEQUENCE if exists public.listtransmission_transmission_id_seq;

CREATE SEQUENCE public.listtransmission_transmission_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.listtransmission_transmission_id_seq
    OWNER TO docker;

-- SEQUENCE: public.list_list_id_seq

DROP SEQUENCE if exists public.list_list_id_seq;

CREATE SEQUENCE public.list_list_id_seq
    INCREMENT 1
    START 11
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.list_list_id_seq
    OWNER TO docker;

-- SEQUENCE: public.file_file_id_seq

DROP SEQUENCE if exists public.file_file_id_seq;

CREATE SEQUENCE public.file_file_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.file_file_id_seq
    OWNER TO docker;

-- SEQUENCE: public.dangerous_good_dangerous_good_id_seq

DROP SEQUENCE if exists public.dangerous_good_dangerous_good_id_seq;

CREATE SEQUENCE public.dangerous_good_dangerous_good_id_seq
    INCREMENT 1
    START 5
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.dangerous_good_dangerous_good_id_seq
    OWNER TO docker;

-- SEQUENCE: public.condition_condition_id_seq

DROP SEQUENCE if exists public.condition_condition_id_seq;

CREATE SEQUENCE public.condition_condition_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.condition_condition_id_seq
    OWNER TO docker;

-- SEQUENCE: public.agent_agent_id_seq

DROP SEQUENCE if exists public.agent_agent_id_seq;

CREATE SEQUENCE public.agent_agent_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.agent_agent_id_seq
    OWNER TO docker;

-- SEQUENCE: public.acceptlist_accept_id_seq

DROP SEQUENCE if exists public.acceptlist_accept_id_seq;

CREATE SEQUENCE public.acceptlist_accept_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.acceptlist_accept_id_seq
    OWNER TO docker;

-- SEQUENCE: public.visit_visit_id_seq

DROP SEQUENCE if exists public.visit_visit_id_seq;

CREATE SEQUENCE public.visit_visit_id_seq
    INCREMENT 1
    START 6
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.visit_visit_id_seq
    OWNER TO docker;

-- SEQUENCE: public.uti_uti_id_seq

DROP SEQUENCE if exists public.uti_uti_id_seq;

CREATE SEQUENCE public.uti_uti_id_seq
    INCREMENT 1
    START 5
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.uti_uti_id_seq
    OWNER TO docker;

-- SEQUENCE: public.user_data_user_id_seq

DROP SEQUENCE if exists public.user_data_user_id_seq;

CREATE SEQUENCE public.user_data_user_id_seq
    INCREMENT 1
    START 2
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.user_data_user_id_seq
    OWNER TO docker;

-- SEQUENCE: public.stop_stop_id_seq

DROP SEQUENCE if exists public.stop_stop_id_seq;

CREATE SEQUENCE public.stop_stop_id_seq
    INCREMENT 1
    START 6
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.stop_stop_id_seq
    OWNER TO docker;

-- SEQUENCE: public.ship_ship_id_seq

DROP SEQUENCE if exists public.ship_ship_id_seq;

CREATE SEQUENCE public.ship_ship_id_seq
    INCREMENT 1
    START 6
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.ship_ship_id_seq
    OWNER TO docker;

-- SEQUENCE: public.seal_seal_id_seq

DROP SEQUENCE if exists public.seal_seal_id_seq;

CREATE SEQUENCE public.seal_seal_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.seal_seal_id_seq
    OWNER TO docker;

-- SEQUENCE: public.role_role_id_seq

DROP SEQUENCE if exists public.role_role_id_seq;

CREATE SEQUENCE public.role_role_id_seq
    INCREMENT 1
    START 5
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.role_role_id_seq
    OWNER TO docker;

-- SEQUENCE: public.reporttransmission_transmission_id_seq

DROP SEQUENCE if exists public.reporttransmission_transmission_id_seq;

CREATE SEQUENCE public.reporttransmission_transmission_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.reporttransmission_transmission_id_seq
    OWNER TO docker;

-- SEQUENCE: public.report_report_id_seq

DROP SEQUENCE if exists public.report_report_id_seq;

CREATE SEQUENCE public.report_report_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.report_report_id_seq
    OWNER TO docker;

-- SEQUENCE: public.refeer_refeer_id_seq

DROP SEQUENCE if exists public.refeer_refeer_id_seq;

CREATE SEQUENCE public.refeer_refeer_id_seq
    INCREMENT 1
    START 5
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.refeer_refeer_id_seq
    OWNER TO docker;

-- SEQUENCE: public.record_record_id_seq

DROP SEQUENCE if exists public.record_record_id_seq;

CREATE SEQUENCE public.record_record_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.record_record_id_seq
    OWNER TO docker;


DROP SEQUENCE if exists public.acceptlist_accept_id_seq;

CREATE SEQUENCE public.acceptlist_accept_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.acceptlist_accept_id_seq
    OWNER TO docker;




CREATE TABLE user_data
(
    user_id bigint NOT NULL DEFAULT nextval('user_data_user_id_seq'::regclass),
    password character varying(255),
    username character varying(255),
    CONSTRAINT user_data_pkey PRIMARY KEY (user_id)
);

CREATE TABLE role
(
    role_id bigint NOT NULL DEFAULT nextval('role_role_id_seq'::regclass),
    role integer,
    CONSTRAINT role_pkey PRIMARY KEY (role_id)
);

CREATE TABLE users_role_detail
(
    user_data_user_id bigint NOT NULL,
    role_id_role_id bigint NOT NULL,
    CONSTRAINT users_role_detail_pkey PRIMARY KEY (user_data_user_id, role_id_role_id),
    CONSTRAINT role_id_role_id FOREIGN KEY (role_id_role_id)
        REFERENCES role (role_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT user_data_user_id FOREIGN KEY (user_data_user_id)
        REFERENCES user_data (user_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE ship
(
    ship_id bigint NOT NULL DEFAULT nextval('ship_ship_id_seq'::regclass),
    call_sign character varying(255),
    flag character varying(255),
    imo_code character varying(255) not null,
    mmsi character varying(255),
    ship_name character varying(255),
    CONSTRAINT ship_pkey PRIMARY KEY (ship_id)
);

CREATE TABLE agent
(
    agent_id bigint NOT NULL DEFAULT nextval('agent_agent_id_seq'::regclass),
    agent_cif character varying(255) not null,
    agent_name character varying(255),
    CONSTRAINT agent_pkey PRIMARY KEY (agent_id)
);

CREATE TABLE location
(
    location_id bigint NOT NULL DEFAULT nextval('location_location_id_seq'::regclass),
    location_name character varying(255),
    removal character varying(255),
    CONSTRAINT location_pkey PRIMARY KEY (location_id)
);

CREATE TABLE refeer
(
    refeer_id bigint NOT NULL DEFAULT nextval('refeer_refeer_id_seq'::regclass),
    active integer,
    air_flow real,
    atmosphere_control integer,
    carbon_dioxide_level real,
    diesel_generator integer,
    humidifier integer,
    instructions character varying(255),
    max_temperature real,
    min_temperature real,
    nitrogen_level real,
    oxygen_level real,
    rel_air_humidity real,
    ship_connected integer,
    terminal_connected integer,
    transport_temperature real,
    um_temperature integer,
    vent_grilles_opening real,
    vent_grilles_state integer,
    CONSTRAINT refeer_pkey PRIMARY KEY (refeer_id)
);

CREATE TABLE visit
(
    visit_id bigint NOT NULL DEFAULT nextval('visit_visit_id_seq'::regclass),
    next_port_code character varying(255),
    visit_ata timestamp without time zone,
    visit_atd timestamp without time zone,
    visit_eta timestamp without time zone,
    visit_etd timestamp without time zone,
    visit_number character varying(255),
    visit_status_id character varying(255),
    agent_id bigint not null,
    ship_id bigint not null,
    CONSTRAINT visit_pkey PRIMARY KEY (visit_id),
    CONSTRAINT agent_id FOREIGN KEY (agent_id)
        REFERENCES agent (agent_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT ship_id FOREIGN KEY (ship_id)
        REFERENCES ship (ship_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE stop
(
    stop_id bigint NOT NULL DEFAULT nextval('stop_stop_id_seq'::regclass),
    stop_ata timestamp without time zone,
    stop_atd timestamp without time zone,
    stop_eta timestamp without time zone,
    stop_etd timestamp without time zone,
    visit_id bigint not null,
    location_id bigint,
    CONSTRAINT stop_pkey PRIMARY KEY (stop_id),
    CONSTRAINT location_id FOREIGN KEY (location_id)
        REFERENCES location (location_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT visit_id FOREIGN KEY (visit_id)
        REFERENCES visit (visit_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE list
(
    list_id bigint NOT NULL DEFAULT nextval('list_list_id_seq'::regclass),
    circuit integer,
    closing_date timestamp without time zone,
    created_date timestamp without time zone,
    operator character varying(255),
    port character varying(255),
    state integer,
    terminal character varying(255),
    type integer,
    updated_date timestamp without time zone,
    version character varying(255),
    created_user bigint not null,
    stop_id bigint not null,
    CONSTRAINT list_pkey PRIMARY KEY (list_id),
    CONSTRAINT created_user FOREIGN KEY (created_user)
        REFERENCES user_data (user_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT stop_id FOREIGN KEY (stop_id)
        REFERENCES stop (stop_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE report
(
    report_id bigint NOT NULL DEFAULT nextval('report_report_id_seq'::regclass),
    circuit integer,
    created_datetime timestamp without time zone,
    list_transmitter character varying(255),
    state integer,
    type integer,
    updated_datetime timestamp without time zone,
    updated_user character varying(255),
    version character varying(255),
    list_id bigint,
    CONSTRAINT report_pkey PRIMARY KEY (report_id),
    CONSTRAINT list_id FOREIGN KEY (list_id)
        REFERENCES list (list_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE file
(
    file_id bigint NOT NULL DEFAULT nextval('file_file_id_seq'::regclass),
    file_name character varying(255),
    file_size bigint NOT NULL,
    file_src oid,
    file_type character varying(255),
    list_id bigint,
    CONSTRAINT file_pkey PRIMARY KEY (file_id),
    CONSTRAINT list_id FOREIGN KEY (list_id)
        REFERENCES list (list_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE listtransmission
(
    transmission_id bigint NOT NULL DEFAULT nextval('listtransmission_transmission_id_seq'::regclass),
    datetime timestamp without time zone,
    receiver character varying(255),
    transmitter character varying(255),
    list_id bigint,
    CONSTRAINT listtransmission_pkey PRIMARY KEY (transmission_id),
    CONSTRAINT list_id FOREIGN KEY (list_id)
        REFERENCES list (list_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE reporttransmission
(
    transmission_id bigint NOT NULL DEFAULT nextval('reporttransmission_transmission_id_seq'::regclass),
    datetime timestamp without time zone,
    receiver character varying(255),
    transmitter character varying(255),
    report_id bigint,
    CONSTRAINT reporttransmission_pkey PRIMARY KEY (transmission_id),
    CONSTRAINT report_id FOREIGN KEY (report_id)
        REFERENCES report (report_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE condition
(
    condition_id bigint NOT NULL DEFAULT nextval('condition_condition_id_seq'::regclass),
    message character varying(255),
    list_id bigint,
    CONSTRAINT condition_pkey PRIMARY KEY (condition_id),
    CONSTRAINT list_id FOREIGN KEY (list_id)
        REFERENCES list (list_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE acceptlist
(
    accept_id bigint NOT NULL DEFAULT nextval('acceptlist_accept_id_seq'::regclass),
    acceptbool boolean NOT NULL,
    list_id bigint,
    CONSTRAINT acceptlist_pkey PRIMARY KEY (accept_id),
    CONSTRAINT list_id FOREIGN KEY (list_id)
        REFERENCES list (list_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE uti
(
    uti_id bigint NOT NULL DEFAULT nextval('uti_uti_id_seq'::regclass),
    bill_of_lading character varying(255),
    booking character varying(255),
    circuit integer,
    extra_back_length integer,
    extra_front_height integer,
    extra_height integer,
    extra_left_width integer,
    extra_right_width integer,
    goods_description character varying(255),
    goods_remarks character varying(255),
    gross_weight real,
    height real,
    instructions character varying(255),
    instructions_remarks character varying(255),
    iso6364_code integer,
    iso6364_compliance integer,
    length real,
    next_port character varying(255),
    operator character varying(255),
    plate character varying(255),
    port character varying(255),
    shipping_company character varying(255),
    shipping_service character varying(255),
    soc integer,
    state integer,
    transshipment_call integer,
    type integer,
    verified_weight real,
    width real,
    list_id bigint,
    refeer_id bigint,
    CONSTRAINT uti_pkey PRIMARY KEY (uti_id),
    CONSTRAINT refeer_id FOREIGN KEY (refeer_id)
        REFERENCES refeer (refeer_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT list_id FOREIGN KEY (list_id)
        REFERENCES list (list_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE seal
(
    seal_id bigint NOT NULL DEFAULT nextval('seal_seal_id_seq'::regclass),
    "number" integer,
    type integer,
    uti_id bigint,
    CONSTRAINT seal_pkey PRIMARY KEY (seal_id),
    CONSTRAINT uti_id FOREIGN KEY (uti_id)
        REFERENCES uti (uti_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE dangerous_good
(
    dangerous_good_id bigint NOT NULL DEFAULT nextval('dangerous_good_dangerous_good_id_seq'::regclass),
    description character varying(255),
    "number" integer,
    packing_type integer,
    type character varying(255),
    uti_id bigint,
    CONSTRAINT dangerous_good_pkey PRIMARY KEY (dangerous_good_id),
    CONSTRAINT uti_id FOREIGN KEY (uti_id)
        REFERENCES uti (uti_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

create view list_view as
	select list.list_id as id, circuit, closing_date, created_date, operator, port, state, terminal, type, updated_date,
		version, user_.username, visit.visit_number, user_.user_id, stop.stop_id, stop.stop_eta, stop.stop_etd
from list list
	join stop stop on stop.stop_id = list.stop_id
	join visit visit on visit.visit_id = stop.visit_id
	join user_data user_ on user_.user_id = list.created_user;

create view uti_view as (
	select uti.uti_id as id, bill_of_lading, booking, circuit, extra_back_length, extra_front_height,
		extra_height, extra_left_width, extra_right_width, goods_description, goods_remarks,
		gross_weight, height, instructions, instructions_remarks, iso6364_code, iso6364_compliance,
		length, next_port, operator, plate, port, shipping_company, shipping_service, soc, state,
		transshipment_call, type, verified_weight, width, list_id as list_id, refeer_id
	from uti as uti
);

create view stop_view as
select stop_.stop_id as stop_id, stop_eta, stop_etd, stop_ata, stop_atd,
	agent.agent_id, agent.agent_cif, agent.agent_name,
	ship.ship_id, call_sign as ship_call_sign, flag as ship_flag, imo_code as ship_imo, mmsi as ship_mmsi, ship_name,
	visit.visit_id, visit_eta, visit_etd, visit_ata,
	visit_atd, visit_status_id, visit_number, next_port_code as visit_next_port_code,
	location_.location_name
from stop stop_
	join visit visit on stop_.visit_id = visit.visit_id
	join agent agent on visit.agent_id = agent.agent_id
	join ship ship on ship.ship_id = visit.ship_id
    join location location_ on location_.location_id = stop_.location_id;