package com.cargaDescarga3.primefaces.converter;

import com.cargaDescarga3.primefaces.enums.EstadoLista;
import com.cargaDescarga3.primefaces.model.bean.ListBeanData;
import com.cargaDescarga3.primefaces.model.data.ListData;
import com.cargaDescarga3.primefaces.model.rest.ListRest;
import com.cargaDescarga3.primefaces.model.view.ListView;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class ListsConverterTests
{
    @Autowired
    ListsConverter listsConverter;

    @BeforeEach
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void convertListViewListToListBeanDataListTest()
    {
        List<ListView> views = new ArrayList<>();
        ListView view = new ListView();
        view.setId(1L);
        view.setOperator("Operator1");
        view.setVisitNumber("201901234");
        views.add(view);

        String operator = "Operator2";
        view = new ListView();
        view.setId(2L);
        view.setOperator(operator);
        view.setVisitNumber("201901234");
        views.add(view);

        List<ListBeanData> listBeanData = listsConverter.convertListViewListToListBeanDataList(views);
        Assertions.assertEquals((long) listBeanData.size(), 2);
        Assertions.assertEquals((long) listBeanData.get(0).getId(), 1);
        Assertions.assertEquals(listBeanData.get(1).getOperator(), operator);
    }

    @Test
    public void convertListViewToListBeanDataTest()
    {
        long id = 1;
        String visitNumber = "201912345";
        String port = "ESALG";
        String username = "francho";
        String terminal = "APMT";

        ListView listView = new ListView();
        listView.setId(id);
        listView.setVisitNumber(visitNumber);
        listView.setPort(port);
        listView.setUsername(username);
        listView.setTerminal(terminal);

        ListBeanData listBeanData = listsConverter.convertListViewToListBeanData(listView);

        Assertions.assertEquals((long) listBeanData.getId(), id);
        Assertions.assertEquals(listBeanData.getTerminal(), terminal);
        Assertions.assertEquals(listBeanData.getPort(), port);
        Assertions.assertEquals(listBeanData.getUsername(), username);
        Assertions.assertEquals(listBeanData.getVisitNumber(), visitNumber);
    }

    @Test
    public void convertListDataListToListRestListTest()
    {
        String operator = "Operator";
        String terminal = "Terminal";
        EstadoLista status = EstadoLista.Pendiente;

        String operator2 = "Operator2";
        String terminal2 = "Terminal2";
        EstadoLista status2 = EstadoLista.Cancelada;

        List<ListData> listDataList = new ArrayList<>();
        ListData listData = new ListData();
        listData.setList_id(1L);
        listData.setStop_id(1L);
        listData.setOperator(operator);
        listData.setState(status);
        listData.setTerminal(terminal);
        listDataList.add(listData);

        listData = new ListData();
        listData.setList_id(2L);
        listData.setStop_id(1L);
        listData.setOperator(operator2);
        listData.setState(status2);
        listData.setTerminal(terminal2);
        listDataList.add(listData);

        List<ListRest> listRestList = listsConverter.convertListDataListToListRestList(listDataList);

        Assertions.assertEquals(listRestList.size(), listDataList.size());
        Assertions.assertEquals(listDataList.get(0).getOperator(), listRestList.get(0).getOperator());
        Assertions.assertEquals(listDataList.get(0).getState(), listRestList.get(0).getState());
        Assertions.assertEquals(listDataList.get(0).getTerminal(), listRestList.get(0).getTerminal());
        Assertions.assertEquals(listDataList.get(1).getOperator(), listRestList.get(1).getOperator());
        Assertions.assertEquals(listDataList.get(1).getState(), listRestList.get(1).getState());
        Assertions.assertEquals(listDataList.get(1).getTerminal(), listRestList.get(1).getTerminal());


    }
}
