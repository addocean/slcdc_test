package com.cargaDescarga3.primefaces.converter;

import com.cargaDescarga3.primefaces.enums.LoginType;
import com.cargaDescarga3.primefaces.model.bean.UserBeanData;
import com.cargaDescarga3.primefaces.model.data.Role;
import com.cargaDescarga3.primefaces.model.data.UserData;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashSet;
import java.util.Set;

@SpringBootTest
public class UsersConverterTests
{
    @Autowired
    UsersConverter usersConverter;

    @Test
    public void convertFromUserDataToUserBeanDataContainerOperatorTest()
    {
        String username = "Alberto";
        Long userId = 1L;
        Set<Role> roles = new HashSet<>();
        Role role = new Role();
        role.setRole(LoginType.Container_Operator);
        roles.add(role);
        role = new Role();
        role.setRole(LoginType.Terminal);
        roles.add(role);

        UserData userData = new UserData();
        userData.setUser_id(userId);
        userData.setUsername(username);
        userData.setRole_id(roles);

        UserBeanData userBeanData = usersConverter.convertFromUserDataToUserBeanData(userData);

        Assertions.assertEquals(userBeanData.getUsername(), username);
        Assertions.assertEquals(userBeanData.getContainer(), true);
    }

    @Test
    public void convertFromUserDataToUserBeanDataNoContainerOperatorTest()
    {
        String username = "Alberto";
        Long userId = 1L;
        Set<Role> roles = new HashSet<>();

        UserData userData = new UserData();
        userData.setUser_id(userId);
        userData.setUsername(username);
        userData.setRole_id(roles);

        UserBeanData userBeanData = usersConverter.convertFromUserDataToUserBeanData(userData);

        Assertions.assertEquals(userBeanData.getUsername(), username);
        Assertions.assertEquals(userBeanData.getContainer(), false);
    }
}
