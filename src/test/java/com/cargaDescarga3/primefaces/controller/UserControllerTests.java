package com.cargaDescarga3.primefaces.controller;

import com.cargaDescarga3.primefaces.model.rest.UserDataRest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserControllerTests
{
    @LocalServerPort
    private int port;

    TestRestTemplate restTemplate = new TestRestTemplate();

    @Test
    public void noValidLoginTest() throws Exception
    {
        UserDataRest user = new UserDataRest();
        user.setUserName("container");
        user.setPassword("_");

        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        HttpEntity<UserDataRest> entity = new HttpEntity<>(user, requestHeaders);
        ResponseEntity<UserDataRest> response = restTemplate.exchange(
                createURLWithPort("/user/"), HttpMethod.POST, entity, UserDataRest.class);
        Assertions.assertEquals(response.getStatusCodeValue(), 404);
    }

    @Test
    public void validLoginTest() throws Exception
    {
        UserDataRest user = new UserDataRest();
        user.setUserName("container");
        user.setPassword("francho");

        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        HttpEntity<UserDataRest> entity = new HttpEntity<>(user, requestHeaders);
        ResponseEntity<UserDataRest> response = restTemplate.exchange(
                createURLWithPort("/user/"), HttpMethod.POST, entity, UserDataRest.class);
        Assertions.assertEquals(response.getStatusCodeValue(), 202);
        Assertions.assertEquals(response.getBody().getToken().substring(0, "Bearer".length()), "Bearer");
    }

    private String createURLWithPort(String uri)
    {
        return "http://localhost:" + port + uri;
    }
}