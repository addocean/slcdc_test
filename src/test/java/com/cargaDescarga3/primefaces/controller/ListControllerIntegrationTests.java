package com.cargaDescarga3.primefaces.controller;

import com.cargaDescarga3.SpringPrimeFacesApplication;
import com.cargaDescarga3.primefaces.model.data.ListData;
import com.cargaDescarga3.primefaces.model.rest.ListRest;
import com.cargaDescarga3.primefaces.model.rest.UserDataRest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.ResourceAccessException;

import java.util.Arrays;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ListControllerIntegrationTests
{
    @LocalServerPort
    private int port;

    private TestRestTemplate restTemplate = new TestRestTemplate();
    private String setCookie;

    @Test
    public void noValidAccessTest()
    {
        String token = "Bearer NotValidToken ";

        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        requestHeaders.setBearerAuth(token);
        saveCookieIfExists(requestHeaders);

        HttpEntity<String> entity = new HttpEntity<>(null, requestHeaders);
        Assertions.assertThrows(ResourceAccessException.class, () ->
                restTemplate.exchange(
                        createURLWithPort("/api/lists/"), HttpMethod.GET, entity, ListData[].class));
    }

    @Test
    public void getListsTest() throws Exception
    {
        // Usuario con listas
        String token = getToken("container", "francho");

        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        requestHeaders.setBearerAuth(token);
        saveCookieIfExists(requestHeaders);

        HttpEntity<String> entity = new HttpEntity<>(null, requestHeaders);
        ResponseEntity<ListData[]> response = restTemplate.exchange(
                createURLWithPort("/api/lists/"), HttpMethod.GET, entity, ListData[].class);
        getCookie(response.getHeaders());
        Assertions.assertEquals(response.getStatusCodeValue(), 200);
        Assertions.assertEquals(response.getBody().length, 1);

        // Ususario sin listas
        token = getToken("container2", "francho");

        requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        requestHeaders.setBearerAuth(token);
        saveCookieIfExists(requestHeaders);

        entity = new HttpEntity<>(null, requestHeaders);
        response = restTemplate.exchange(
                createURLWithPort("/api/lists/"), HttpMethod.GET, entity, ListData[].class);
        getCookie(response.getHeaders());
        Assertions.assertEquals(response.getStatusCodeValue(), 200);
        Assertions.assertEquals(response.getBody().length, 0);
    }

    @Test
    public void addListTest()
    {
        // Usuario con listas
        String token = getToken("container3", "francho");

        // Lista con el stop bien
        ListRest listRest = new ListRest();
        listRest.setStopId(1L);

        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        requestHeaders.setBearerAuth(token);
        saveCookieIfExists(requestHeaders);

        HttpEntity<String> entityString = new HttpEntity<>(null, requestHeaders);
        ResponseEntity<ListData[]> responseListData = restTemplate.exchange(
                createURLWithPort("/api/lists/"), HttpMethod.GET, entityString, ListData[].class);
        getCookie(responseListData.getHeaders());
        Assertions.assertEquals(responseListData.getStatusCodeValue(), 200);
        int elementsInList = responseListData.getBody().length;

        HttpEntity<ListRest> entity = new HttpEntity<>(listRest, requestHeaders);
        ResponseEntity<ListData> response = restTemplate.exchange(
                createURLWithPort("/api/lists/"), HttpMethod.POST, entity, ListData.class);
        getCookie(response.getHeaders());
        Assertions.assertEquals(response.getStatusCodeValue(), 200);
        long listId = response.getBody().getList_id();
        Assertions.assertTrue(listId >= 1);

        entityString = new HttpEntity<>(null, requestHeaders);
        responseListData = restTemplate.exchange(
                createURLWithPort("/api/lists/"), HttpMethod.GET, entityString, ListData[].class);
        getCookie(responseListData.getHeaders());
        Assertions.assertEquals(responseListData.getStatusCodeValue(), 200);
        int elementsInList2 = responseListData.getBody().length;
        Assertions.assertEquals(elementsInList + 1, elementsInList2);

        // Recuperamos la lista y tiene que tener el mismo valor
        entity = new HttpEntity<>(null, requestHeaders);
        ResponseEntity<ListRest> listRestResponse = restTemplate.exchange(
                createURLWithPort("/api/lists/" + listId), HttpMethod.GET, entity, ListRest.class);
        getCookie(listRestResponse.getHeaders());
        Assertions.assertEquals(listRestResponse.getStatusCodeValue(), 200);
        Assertions.assertEquals((long) listRestResponse.getBody().getList_id(), listId);
    }

    @Test
    public void addListNoValidStopTest()
    {
        // Usuario con listas
        String token = getToken("container3", "francho");

        // Otra lista con el stop mal
        ListRest listRest = new ListRest();
        listRest.setStopId(200L);

        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        requestHeaders.setBearerAuth(token);
        saveCookieIfExists(requestHeaders);

        HttpEntity<ListRest> entity = new HttpEntity<>(listRest, requestHeaders);
        ResponseEntity<ListData> response = restTemplate.exchange(
                createURLWithPort("/api/lists/"), HttpMethod.POST, entity, ListData.class);
        getCookie(response.getHeaders());
        Assertions.assertEquals(response.getStatusCodeValue(), 500);
    }

    private String getToken(String username, String password)
    {
        UserDataRest user = new UserDataRest();
        user.setUserName(username);
        user.setPassword(password);

        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        HttpEntity<UserDataRest> entity = new HttpEntity<>(user, requestHeaders);
        ResponseEntity<UserDataRest> response = restTemplate.exchange(
                createURLWithPort("/user/"), HttpMethod.POST, entity, UserDataRest.class);

        String token = response.getBody().getToken();

        return token;
    }

    private String createURLWithPort(String uri)
    {
        return "http://localhost:" + port + uri;
    }

    private void saveCookieIfExists(HttpHeaders requestHeaders)
    {
        if ((setCookie != null) && !setCookie.isEmpty())
        {
            if (requestHeaders.get(requestHeaders.COOKIE) == null)
            {
                requestHeaders.add(requestHeaders.COOKIE, setCookie);
            }
        }
    }

    private void getCookie(HttpHeaders headers)
    {
        if ((setCookie == null) || setCookie.isEmpty())
        {
            setCookie = headers.getFirst(headers.SET_COOKIE);
        }
    }
}
