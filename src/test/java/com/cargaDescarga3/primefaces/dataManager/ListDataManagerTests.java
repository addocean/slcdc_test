package com.cargaDescarga3.primefaces.dataManager;

import com.cargaDescarga3.SpringPrimeFacesApplication;
import com.cargaDescarga3.primefaces.converter.ListsConverter;
import com.cargaDescarga3.primefaces.enums.EstadoLista;
import com.cargaDescarga3.primefaces.exceptions.NoValidStopException;
import com.cargaDescarga3.primefaces.model.bean.ListBeanData;
import com.cargaDescarga3.primefaces.model.bean.StopBeanData;
import com.cargaDescarga3.primefaces.model.data.ListData;
import com.cargaDescarga3.primefaces.model.data.UserData;
import com.cargaDescarga3.primefaces.model.rest.ListRest;
import com.cargaDescarga3.primefaces.model.view.ListView;
import com.cargaDescarga3.primefaces.repository.ListRepository;
import com.cargaDescarga3.primefaces.repository.ListViewRepository;
import com.cargaDescarga3.primefaces.repository.UserRepository;
import com.cargaDescarga3.primefaces.services.*;
import org.dozer.DozerBeanMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ContextConfiguration(classes = SpringPrimeFacesApplication.class)
@SpringBootTest
public class ListDataManagerTests
{
    @Mock
    private ListViewRepository listViewRepository;

    @Mock
    private IUserLoggedService userLoggedDataService;

    @InjectMocks
    private IListDataService listDataService = new ListDataService();

    @Spy
    ListsConverter listConverter;

    @Spy
    DozerBeanMapper dozerBeanMapper;

    @Mock
    StopDataService stopDataService;

    @Mock
    UserRepository userRepository;

    @Mock
    ListRepository listRepository;

    @BeforeEach
    public void setup()
    {
        MockitoAnnotations.initMocks(this);
        listConverter.setDozerBeanMapper(dozerBeanMapper);
    }

    @Test
    public void getMyCreatedListsBeansList()
    {
        UserData user = new UserData();
        user.setUser_id(1L);

        when(userLoggedDataService.getLoggedUser()).thenReturn(user);

        List<ListView> listDataList = new ArrayList<>();
        ListView listData = new ListView();
        listData.setId(1L);
        listData.setUserId(1L);
        listDataList.add(listData);

        listData = new ListView();
        listData.setId(2L);
        listData.setUserId(1L);
        listDataList.add(listData);

        when(listViewRepository.getByUserId(1L)).thenReturn(listDataList);

        listConverter.setDozerBeanMapper(dozerBeanMapper);
        List<ListBeanData> result = listDataService.getMyCreatedListsBeans();

        assertEquals(2, result.size());

        assertEquals((long) result.get(0).getId(), 1L);
        assertEquals((long) result.get(1).getId(), 2L);
    }

    @Test
    public void createNewListFromDataTest() throws NoValidStopException
    {
        UserData user = new UserData();
        user.setUser_id(1L);
        user.setUsername("pepe");

        StopBeanData stopBeanData = new StopBeanData();
        stopBeanData.setStopId(1L);
        stopBeanData.setAgentName("Manolo");

        ListView listView = new ListView();
        listView.setId(1L);
        listView.setState(EstadoLista.Pendiente);
        listView.setUsername("pepe");

        ListData listData = new ListData();
        listData.setList_id(1L);

        when(userRepository.getOne(1L)).thenReturn(user);
        when(stopDataService.getStopBeanFromId(1L)).thenReturn(stopBeanData);
        when(listViewRepository.getOne(1L)).thenReturn(listView);
        when(listRepository.save(any(ListData.class))).thenReturn(listData);

        ListRest listRest = new ListRest();
        listRest.setStopId(1L);

        ListRest listRestReturned = listDataService.createNewListFromRestController(listRest, 1L);
        Assertions.assertEquals(listRestReturned.getState().toString(), EstadoLista.Pendiente.toString());
        Assertions.assertEquals(listRestReturned.getUsername(), "pepe");
        // Assertions.assertEquals(listData.getAgentName(), "Manolo");
    }

    @Test
    public void createNewListFromDataTestWithNoValidStop() throws NoValidStopException
    {
        UserData user = new UserData();
        user.setUser_id(1L);
        user.setUsername("pepe");

        when(userRepository.getOne(1L)).thenReturn(user);
        when(stopDataService.getStopBeanFromId(1L)).thenThrow(NoValidStopException.class);

        ListRest listRest = new ListRest();
        listRest.setStopId(1L);

        Assertions.assertThrows(NoValidStopException.class, () ->
                listDataService.createNewListFromRestController(listRest, 1L));
    }
}
