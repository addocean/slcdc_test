package com.cargaDescarga3.primefaces.dataManager;

import com.cargaDescarga3.SpringPrimeFacesApplication;
import com.cargaDescarga3.primefaces.converter.UtisConverter;
import com.cargaDescarga3.primefaces.model.bean.UtiBeanData;
import com.cargaDescarga3.primefaces.model.data.Uti;
import com.cargaDescarga3.primefaces.model.view.UtiView;
import com.cargaDescarga3.primefaces.repository.UtiRepository;
import com.cargaDescarga3.primefaces.repository.UtiViewRepository;
import com.cargaDescarga3.primefaces.services.IUtiDataService;
import com.cargaDescarga3.primefaces.services.UtiDataService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@ContextConfiguration(classes = SpringPrimeFacesApplication.class)
@SpringBootTest
public class UtiDataServiceTests
{
    @InjectMocks
    private IUtiDataService utiDataService = new UtiDataService();

    @Mock
    private UtiRepository utiRepository;

    @Mock
    private UtiViewRepository utiViewRepository;

    @SpyBean
    private UtisConverter utisConverter;

    @BeforeEach
    public void setup()
    {
        MockitoAnnotations.initMocks(this);

        Uti uti1 = new Uti();
        uti1.setUti_id(1L);
        List<Uti> utis = new ArrayList<>();
        utis.add(uti1);

        Uti uti2 = new Uti();
        uti2.setUti_id(2L);
        utis.add(uti2);

        when(utiRepository.findAll()).thenReturn(utis);
        when(utiRepository.getOne(1L)).thenReturn(uti1);
        when(utiRepository.save(Mockito.any(Uti.class))).thenReturn(uti1);

        List<UtiView> utiViews = new ArrayList<>();
        UtiView utiView = new UtiView();
        utiView.setId(1L);
        utiViews.add(utiView);
        utiView = new UtiView();
        utiView.setId(2L);
        utiViews.add(utiView);
        when(utiViewRepository.getBylistId(1L)).thenReturn(utiViews);
    }

    @Test
    public void getUtisForListTest()
    {
        List<UtiBeanData> utisForList = utiDataService.getUtisForList(1L);
        Assertions.assertEquals(utisForList.size(), 2);
        Assertions.assertEquals((long) utisForList.get(0).getId(), 1L);
        Assertions.assertEquals((long) utisForList.get(1).getId(), 2L);
    }
}
