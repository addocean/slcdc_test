var available_printers = null;
var selected_category = null;
var default_printer = null;
var selected_printer = null;
var format_start = "^XA^LL200^FO80,50^A0N36,36^FD";
var format_end = "^FS^XZ";
var default_mode = true;

function setup_web_print()
{
	default_mode = true;
	selected_printer = null;
	available_printers = null;
	selected_category = null;
	default_printer = null;
	list_data = null;
	indexPrint = 0;
	
	BrowserPrint.getDefaultDevice('printer', function(printer)
	{
		default_printer = printer
		if((printer != null) && (printer.connection != undefined))
		{
			selected_printer = printer;
		}
		BrowserPrint.getLocalDevices(function(printers)
			{
				available_printers = printers;
				var printers_available = false;
				if (printers != undefined)
				{
					for(var i = 0; i < printers.length; i++)
					{
						if (printers[i].connection == 'usb')
						{
							printers_available = true;
						}
					}
				}
				
				if(!printers_available)
				{
                    alert('No hay impresoras disponibles')
					return;
				}
			}, undefined, 'printer');
	}, 
	function(error_response)
	{
		showBrowserPrintNotFound();
	});
};

function printText()
{
    textData = document.getElementById("admin:textDemo").value;
    alert(textData);
    textData = format_start + textData + format_end;
    sendDemo(textData);
};

function showBrowserPrintNotFound()
{
	alert("An error occured while attempting to connect to your Zebra Printer. You may not have Zebra Browser Print installed, or it may not be running. Install Zebra Browser Print, or start the Zebra Browser Print Service, and try again.");

};
function sendData(data)
{
    list_data = data.split("@@&&@@");
    indexPrint = 0;
//    printData(list_data[indexPrint]);
    printData(data);
};

function sendDemo(data)
{
    printDemo(data);
};

function printDemo(data)
{
     checkPrinterStatus( function (text){
            if (text == "Ready to Print")
           {
               selected_printer.send(data, demoComplete, printerError);
           }
           else
           {
               printerError(text);
           }
       });
}

function sendRecovered(data){
    printRecovered(data);
}

function printRecovered(data)
{
    checkPrinterStatus( function (text){
            if (text == "Ready to Print")
            {
                selected_printer.send(data, printRecoveredComplete, printerError);
            }
            else
            {
                printerError(text);
            }
        });
}

function printData(data)
{
    //slep(600);
    checkPrinterStatus( function (text){
            if (text == "Ready to Print")
            {
                selected_printer.send(data, printComplete, printerError);
            }
            else
            {
                printerError(text);
            }
        });
}

function sendConfig(data)
{
    printConfig(data);
};

function printConfig(data)
{
    checkPrinterStatus( function (text){
            if (text == "Ready to Print")
            {
                selected_printer.send(data, configComplete, printerError);
            }
            else
            {
                printerError(text);
            }
        });
}

function sleep(milliseconds)
{
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}
function checkPrinterStatus(finishedFunction)
{
	selected_printer.sendThenRead("~HQES", 
				function(text){
						var that = this;
						var statuses = new Array();
						var ok = false;
						var is_error = text.charAt(70);
						var media = text.charAt(88);
						var head = text.charAt(87);
						var pause = text.charAt(84);
						// check each flag that prevents printing
						if (is_error == '0')
						{
							ok = true;
							statuses.push("Ready to Print");
						}
						if (media == '1')
							statuses.push("Paper out");
						if (media == '2')
							statuses.push("Ribbon Out");
						if (media == '4')
							statuses.push("Media Door Open");
						if (media == '8')
							statuses.push("Cutter Fault");
						if (head == '1')
							statuses.push("Printhead Overheating");
						if (head == '2')
							statuses.push("Motor Overheating");
						if (head == '4')
							statuses.push("Printhead Fault");
						if (head == '8')
							statuses.push("Incorrect Printhead");
						if (pause == '1')
							statuses.push("Printer Paused");
						if ((!ok) && (statuses.Count == 0))
							statuses.push("Error: Unknown Error");
						finishedFunction(statuses.join());
			}, printerError);
};

function printComplete()
{
    PF('finImp').show();
}

function printRecoveredComplete()
{
    PF('endRecover').show();
}

function configComplete()
{
    alert("Config complete");
}

function demoComplete()
{
    alert("Complete!");
}

function printerError(text)
{
	alert("An error occurred while printing. Please try again." + text);
}
function trySetupAgain()
{
	setup_web_print();
}


