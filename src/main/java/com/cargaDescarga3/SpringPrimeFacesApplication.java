package com.cargaDescarga3;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class SpringPrimeFacesApplication extends SpringBootServletInitializer
{
    private Logger logger = LoggerFactory.getLogger(SpringPrimeFacesApplication.class);

    public static void main(String[] args)
    {
        SpringApplication.run(SpringPrimeFacesApplication.class, args);
    }

    @PostConstruct
    void postContruct()
    {
        logger.info("Initializing Application SLCDC");
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application)
    {
        logger.info("Configuring Application SLCDC");

        /*
        Map<String, Object> props = new HashMap<>();

        props.put("spring.config.location", "classpath:/,/home/bitnami/stack/wildfly/standalone/configuration/");
        application.properties(props);

        logger.info("Configuring Application from " + props.get("spring.config.location"));
        */

        return application.sources(SpringPrimeFacesApplication.class);
    }
}
