package com.cargaDescarga3.primefaces.configuration;

import com.cargaDescarga3.primefaces.model.data.ListData;
import org.dozer.DozerConverter;

public class UtiCustomConverter extends DozerConverter<Long, ListData>
{
    public UtiCustomConverter()
    {
        super(Long.class, ListData.class);
    }

    @Override
    public ListData convertTo(Long listId1, ListData listId2)
    {
        return null;
    }

    @Override
    public Long convertFrom(ListData listId1, Long listId2)
    {
        return listId1.getList_id();
    }
}