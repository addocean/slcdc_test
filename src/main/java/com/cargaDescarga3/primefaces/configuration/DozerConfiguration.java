package com.cargaDescarga3.primefaces.configuration;

import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.List;

@Configuration
public class DozerConfiguration
{
    private Logger logger = LoggerFactory.getLogger(DozerConfiguration.class);

    @Bean(name = "org.dozer.Mapper")
    public DozerBeanMapper dozerBean()
    {
        logger.info("Creating new dozerBean");

        List<String> mappingFiles = Arrays.asList("custom-dozer-bean-mappings.xml");

        DozerBeanMapper dozerBean = new DozerBeanMapper();
        dozerBean.setMappingFiles(mappingFiles);
        logger.info("Created new dozerBean " + dozerBean);

        return dozerBean;
    }
}
