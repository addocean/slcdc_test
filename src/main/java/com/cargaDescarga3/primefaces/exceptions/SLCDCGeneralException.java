package com.cargaDescarga3.primefaces.exceptions;

public class SLCDCGeneralException extends RuntimeException
{
    public SLCDCGeneralException(String s)
    {
        super("Ha ocurrido un problema: " + s);
    }
}
