package com.cargaDescarga3.primefaces.exceptions;

public class UtiNotFoundException extends RuntimeException
{
    public UtiNotFoundException(Long id)
    {
        super("No pude encontrar el uti con id: " + id);
    }
}
