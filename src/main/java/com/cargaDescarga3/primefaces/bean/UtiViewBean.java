package com.cargaDescarga3.primefaces.bean;

import com.cargaDescarga3.primefaces.model.bean.UtiBeanData;
import com.cargaDescarga3.primefaces.enums.Afirmacion;
import com.cargaDescarga3.primefaces.exceptions.SLCDCGeneralException;
import com.cargaDescarga3.primefaces.presenter.UtiViewBeanPresenter;
import com.cargaDescarga3.primefaces.utils.NavigatorUtil;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.Serializable;

@Data
@Named
@ViewScoped
public class UtiViewBean implements Serializable
{
    //edit
    private UtiBeanData uti_edit;
    @Autowired
    private UtiViewBeanPresenter presenter;

    @PostConstruct
    public void init() throws IOException
    {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String parameterOne = request.getParameter("id");
        if (parameterOne != null)
        {
            Long utiIdParameter = Long.parseLong(parameterOne);
            uti_edit = presenter.getUtiBean(utiIdParameter);
        }

        parameterOne = request.getParameter("listId");
        if (parameterOne != null)
        {
            Long listId = Long.parseLong(parameterOne);
            uti_edit = new UtiBeanData();
            uti_edit.setListId(listId);
        }
    }

    public java.util.List<UtiBeanData> getUtis()
    {
        return presenter.getAllUtiBeans();
    }

    public void addAction() throws IOException
    {

        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();

        String parameterOne = request.getParameter("listId");

        presenter.createNewUti(uti_edit);

        NavigatorUtil.RedirectToListDetails(uti_edit.getListId());
    }


    public UtiBeanData getUti(Long id)
    {
        UtiBeanData result;

        // Ir a buscar la uti directamente con el id
        if (id != null)
        {
            uti_edit = presenter.getUtiBean(id);
            result = uti_edit;
        }
        else
        {
            throw new SLCDCGeneralException("No se dió ningun id a buscar");
        }
        return result;
    }

    public Boolean showIso(Afirmacion iso)
    {
        Boolean res;
        res = iso == Afirmacion.Si;
        return res;
    }

    public void editAction() throws IOException
    {
        presenter.saveUtiBean(uti_edit);

        NavigatorUtil.RedirectToListDetails(uti_edit.getListId());
    }

    public void deleteAction(Long id) throws IOException
    {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();

        String parameterOne = request.getParameter("listId");

        presenter.removeUtiBean(id);

        NavigatorUtil.RedirectToListDetails(Long.parseLong(parameterOne));
    }

    public boolean isEditingMode()
    {
        boolean result = false;
        if ((uti_edit != null) && (uti_edit.getId() != null))
        {
            result = true;
        }
        return result;
    }
}
