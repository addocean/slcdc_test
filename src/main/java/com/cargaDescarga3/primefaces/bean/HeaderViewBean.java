package com.cargaDescarga3.primefaces.bean;


import com.cargaDescarga3.primefaces.model.bean.UserBeanData;
import com.cargaDescarga3.primefaces.services.IUserLoggedService;
import com.cargaDescarga3.primefaces.utils.NavigatorUtil;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.annotation.SessionScope;

import javax.annotation.PostConstruct;
import javax.inject.Named;

@Data
@Named
@SessionScope
public class HeaderViewBean
{
    private Logger logger = LoggerFactory.getLogger(HeaderViewBean.class);
    private UserBeanData user;

    @Autowired
    private IUserLoggedService userLoggedDataManager;

    private String headerURL = NavigatorUtil.GetHeaderURL();

    @PostConstruct
    void init()
    {
        logger.info("Creando objecto " + this);
        user = userLoggedDataManager.getLoggerUserBeanData();
    }
}
