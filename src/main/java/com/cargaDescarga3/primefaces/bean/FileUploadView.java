package com.cargaDescarga3.primefaces.bean;

import com.cargaDescarga3.primefaces.presenter.FileUploadPresenter;
import com.cargaDescarga3.primefaces.utils.NavigatorUtil;
import lombok.Data;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.ManagedBean;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.io.IOException;

@ManagedBean
@Data
public class FileUploadView
{
    @Autowired
    private FileUploadPresenter fileUploadPresenter;

    private UploadedFile file;

    public void upload(long listId)
    {
        if (file != null)
        {
            FacesMessage message = new FacesMessage("Succesful", file.getFileName() + " is uploaded.");
            FacesContext.getCurrentInstance().addMessage(null, message);

            fileUploadPresenter.fileUploaded (file, listId);
        }
    }

    public void handleFileUpload(FileUploadEvent event)
    {
        Long listId = (Long) event.getComponent().getAttributes().get("listId");

        FacesMessage msg = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, msg);

        fileUploadPresenter.fileUploaded (event.getFile(), listId);
    }
}
