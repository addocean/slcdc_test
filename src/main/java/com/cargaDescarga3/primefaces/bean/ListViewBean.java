package com.cargaDescarga3.primefaces.bean;

import com.cargaDescarga3.primefaces.model.bean.ListBeanData;
import com.cargaDescarga3.primefaces.model.bean.MainSummaryBeanData;
import com.cargaDescarga3.primefaces.model.bean.StopBeanData;
import com.cargaDescarga3.primefaces.enums.EstadoLista;
import com.cargaDescarga3.primefaces.enums.LoginType;
import com.cargaDescarga3.primefaces.enums.TipoLista;
import com.cargaDescarga3.primefaces.exceptions.SLCDCGeneralException;
import com.cargaDescarga3.primefaces.exceptions.NoValidStopException;
import com.cargaDescarga3.primefaces.presenter.ListViewBeanPresenter;
import com.cargaDescarga3.primefaces.utils.NavigatorUtil;
import lombok.Data;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.UploadedFile;
import org.primefaces.model.Visibility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@Named
@ViewScoped
@ManagedBean
public class ListViewBean implements Serializable
{
    @Autowired
    ListViewBeanPresenter listViewBeanPresenter;

    //TODO: No he terminado aquí. Esto de aquí abajo tengo que quitarlo y modificar todo esto para que funcione
    /*private ListData list_edit;
    private ListData create_list;
    private Long stop_id;*/
    //estadisticas

    private MainSummaryBeanData mainSummaryBeanData;
    //list
    private ListBeanData selected_list;
    //envia
    String updated_user;
    private LoginType sendTo;
    private String sendToPerson;
    //listas
    private java.util.List<ListBeanData> lists;
    private java.util.List<StopBeanData> stops;
    //private java.util.List<File> files;
    //condition
    private String conditionMessage;
    //cambio de stop
    private Long stop_a_buscar;
    private StopBeanData selectedStop;
    //Mensaje de error
    private String errMessage;
    private Logger logger = LoggerFactory.getLogger(ListViewBean.class);
    private Boolean[] visibleColumns;

    @PostConstruct
    public void init() throws IOException
    {
        visibleColumns = listViewBeanPresenter.getVisibleColumns();

        mainSummaryBeanData = new MainSummaryBeanData();

        boolean userLogged = listViewBeanPresenter.isAnyUserLogged();
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String parameterOne = request.getParameter("listId");
        if (parameterOne != null)
        {
            Long id = Long.valueOf(parameterOne);

            selected_list = listViewBeanPresenter.createListBeanData(id);
            selectedStop = selected_list.getStopBeanData();

            stop_a_buscar = selectedStop.getStopId();
        }
        else
        {
            selected_list = new ListBeanData();
            selectedStop = new StopBeanData();
        }

        if (lists == null)
        {
            lists = listViewBeanPresenter.getMyCreatedListsBeans();
        }
        if (stops == null)
        {
            stops = listViewBeanPresenter.getAllStops();
        }
        /*
        if (files == null)
        {
            files = listViewBeanPresenter.encontrarTodo();
        }
         */
    }

    //se usa
    public void addActionFile() throws IOException
    {
        boolean containerOperator = listViewBeanPresenter.hasLoggedUserRole("Container_Operator");
        if (containerOperator)
        {
            /*
            File file_to_delete = new File();
            for (File f : files)
            {
                if (f.getListId() == listViewBeanPresenter.findListById(selected_list.getId()))
                {
                    file_to_delete = f;
                }
            }

            listViewBeanPresenter.elimina(file_to_delete);
            File newFile = new File();
            newFile.setFile_name(file_src.getFileName());
            newFile.setFile_type(file_src.getContentType());
            newFile.setFile_size(file_src.getSize());
            newFile.setFile_src(file_src.getContents());
            newFile.setListId(listViewBeanPresenter.findListById(selected_list.getId()));
            listViewBeanPresenter.saveListData(newFile);
            String url = "listEdit.xhtml?listId=" + newFile.getListId().getListId();
            FacesContext.getCurrentInstance().getExternalContext().redirect(url);
            */
        }
        else
        {
            NavigatorUtil.RedirectToLists();
        }
    }

    public void cancelFile()
    {
        /*
        File file_to_delete = new File();
        for (File f : files)
        {
            if (f.getListId() == listViewBeanPresenter.findListById(selected_list.getId()))
            {
                file_to_delete = f;
            }
        }
        listViewBeanPresenter.elimina(file_to_delete);
        files = listViewBeanPresenter.encontrarTodo();

         */
    }

    //se usa
    public void downloadFile() throws IOException
    {
        /*
        java.io.File file_res;
        File file_todo = null;
        for (File f : files)
        {
            if (f.getListId() == listViewBeanPresenter.findListById(selected_list.getId()))
            {
                file_todo = f;
            }
        }
        if (file_todo != null)
        {
            //file
            file_res = new java.io.File(file_todo.getFile_name());
            FileUtils.writeByteArrayToFile(file_res, file_todo.getFile_src());
            //Content-Type
            String contentType = URLConnection.guessContentTypeFromName(file_res.getName());
            //Length
            String length = String.valueOf(file_res.length());
            //disposition
            String disposition = "attachment;filename=\"" + file_res.getName() + "\"";
            //data
            byte[] data = FileUtils.readFileToByteArray(file_res);
            //Enviar datos:
            FacesContext facesContext = FacesContext.getCurrentInstance();
            ExternalContext externalContext = facesContext.getExternalContext();
            externalContext.setResponseHeader("Content-Type", contentType);
            externalContext.setResponseHeader("Content-Length", length);
            externalContext.setResponseHeader("Content-Disposition", disposition);
            OutputStream outputStream = externalContext.getResponseOutputStream();
            outputStream.write(data);
            facesContext.responseComplete();
        }


         */
    }

    public java.util.List<ListBeanData> getLists()
    {
        if (lists == null)
        {
            return listViewBeanPresenter.getMyCreatedListsBeans();
        }

        int listPrep = 0;
        int listClosedPend = 0;

        for (ListBeanData l : lists)
        {
            if (l.getState() == EstadoLista.Pendiente)
            {
                listPrep++;
            }
            else
            {
                listClosedPend++;
            }
        }

        mainSummaryBeanData.setPreparingList(listPrep);
        mainSummaryBeanData.setClosedLists(listClosedPend);

        return lists;
    }

    public ListBeanData getList(Long id)
    {
        if (id == null)
        {
            throw new SLCDCGeneralException("No se dió ningun id a buscar");
        }
        if (lists != null)
        {
            selected_list = listViewBeanPresenter.createListBeanData(id);
            return selected_list;
        }
        return null;
    }

    public List<ListBeanData> getMyAcceptedLists()
    {

        List<ListBeanData> res = new ArrayList<>();

        /*
        List<ListData> myLists = getMyLists();

        for (ListData l : myLists)
        {
            AcceptList acceptList = listViewBeanPresenter.encontrarPorLista(l.getListId());
            if (acceptList != null)
            {
                if (acceptList.getAcceptBool())
                {
                    ListData lista = acceptList.getListId();
                    res.add(lista);
                }
            }
            else
            {
                AcceptList accept = new AcceptList();
                accept.setListId(l);
                accept.setAcceptBool(false);
                listViewBeanPresenter.guardarAcceptList(accept);
            }
        }

         */
        return res;
    }

    //se usa
    public List<ListBeanData> getMyNotAcceptedLists()
    {

        List<ListBeanData> res = new ArrayList<>();
        /*
        List<ListData> myLists = getMyLists();

        for (ListData l : myLists)
        {
            AcceptList acceptList = listViewBeanPresenter.encontrarPorLista(l.getListId());
            if (acceptList != null)
            {
                if (!acceptList.getAcceptBool())
                {
                    ListData lista  = acceptList.getListId();
                    res.add(lista);
                }
            }
            else
            {
                AcceptList accept = new AcceptList();
                accept.setListId(l);
                accept.setAcceptBool(false);
                listViewBeanPresenter.guardarAcceptList(accept);
                ListData lista  = acceptList.getListId();
                res.add(lista);
            }
        }

         */
        return res;
    }

    private List<ListBeanData> getMyLists()
    {
        List<ListBeanData> res = new ArrayList<>();

        // TODO: Es el dataManager correspondiente el que debe saber el usuario y devolver la lista adecuada
        /*
        listViewBeanPresenter.getMyCreatedListsRest();

        if (userDataManagerSpringSecurity.getRolesFromLoggerUser().contains(roleDataManager.findByName("Vessel Operator")))
        {
            for (List l : lists)
            {
                if (l.getOperator() != null)
                {
                    if (l.getOperator().equals(userDataManagerSpringSecurity.getUserLoggedName()))
                    {
                        res.add(l);
                    }
                }
            }
        }
        else if (userDataManagerSpringSecurity.getRolesFromLoggerUser().contains(roleDataManager.findByName("Terminal")))
        {
            for (List l : lists)
            {
                if (l.getTerminal() != null)
                {
                    if (l.getTerminal().equals(userDataManagerSpringSecurity.getUserLoggedName()))
                    {
                        res.add(l);
                    }
                }
            }
        }else{
            for(List l : lists){
                if(l.getCreated_user().getUsername().equals(userDataManagerSpringSecurity.getUserLoggedName())){
                    res.add(l);
                }
            }
        }
        */
        return res;
    }

    public void addAction() throws IOException, NoValidStopException
    {
        if (listViewBeanPresenter.hasLoggedUserRole("Container_Operator"))
        {

            if (selectedStop != null)
            {
                String username = listViewBeanPresenter.getUserLoggedName();
                updated_user = username;

                // Se ha utilizado el mismo método en la ventana que en el restController. Método del DataController
                Long newListId = listViewBeanPresenter.createNewListFromBean(selected_list, selectedStop.getStopId());

                lists = listViewBeanPresenter.getMyCreatedListsBeans();

                NavigatorUtil.RedirectToListDetails(newListId);
            }
            else
            {
                // En vez de lanzar excepciones, mostrar un mensaje de error al usuario. No hay excepción, está controlado por la vista
                // Mostrar error de STOP no válido. En la ventana
                errMessage = "No se ha especificado una parada. Por favor, indíquela e intente de nuevo.";
            }
        }
        else
        {
            // En vez de lanzar excepciones, mostrar un mensaje de error al usuario. No hay excepción, está controlado por la vista
            // Mostrar error de tipo de usuario no válido. En la ventana
            errMessage = "No eres un usuario válido para realizar esta acción. Por favor, inicia sesión con un usuario que tenga permisos de 'Container Operator e intente de nuevo.'";
            //FacesContext.getCurrentInstance().getExternalContext().redirect("lists.xhtml");
        }
    }

    public void editAction() throws IOException
    {
        if (listViewBeanPresenter.isAnyUserLogged())
        {
            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            String parameterOne = request.getParameter("listId");
            if (parameterOne != null)
            {
                listViewBeanPresenter.createNewListFromBean(selected_list, selectedStop.getStopId());

                NavigatorUtil.RedirectToListDetails(Long.parseLong(parameterOne));
            }
            else
            {
                NavigatorUtil.RedirectToLists();
            }
        }
        else
        {
            NavigatorUtil.RedirectToLogin();
        }
    }

    private void acceptAction(Long id) throws IOException
    {
        /*
        AcceptList acceptList = listViewBeanPresenter.encontrarPorLista(id);

        if (acceptList != null)
        {
            if (!acceptList.getAcceptBool())
            {
                acceptList.setAcceptBool(true);
                listViewBeanPresenter.editaAcceptList(acceptList);
            }
        }
        else
        {
            AcceptList accept = new AcceptList();
            accept.setListId(listViewBeanPresenter.findListById(id));
            accept.setAcceptBool(false);
            listViewBeanPresenter.guardarAcceptList(accept);
        }

        String url = "my_lists.xhtml";
        FacesContext.getCurrentInstance().getExternalContext().redirect(url);

         */
    }

    public void acceptList(Long id) throws IOException
    {
        /*
        if (listViewBeanPresenter.isAnyUserLogged())
        {
            List<ListData> my_accepted_lists = getMyAcceptedLists();
            List<Integer> cuenta = countMyLists(my_accepted_lists);

            int my_lists_carga = 0;
            int my_lists_descarga = 0;

            my_lists_carga = cuenta.get(0);
            my_lists_descarga = cuenta.get(1);

            ListBeanData list = getList(id);
            if (my_lists_carga >= 1 && list.getCircuit() == TipoLista.Carga)
            {
                //addMessage("Restriccion", "No pueden haber más de dos listas de carga o descarga aceptados a la vez");
                String url = "my_lists.xhtml";
                FacesContext.getCurrentInstance().getExternalContext().redirect(url);

            }
            else if (my_lists_carga < 1 && list.getCircuit() == TipoLista.Carga)
            {
                acceptAction(id);
            }
            if (my_lists_descarga >= 1 && list.getCircuit() == TipoLista.Descarga)
            {
                //addMessage("Restriccion", "No pueden haber más de dos listas de carga o descarga aceptados a la vez");
                String url = "my_lists.xhtml";
                FacesContext.getCurrentInstance().getExternalContext().redirect(url);

            }
            else if (my_lists_descarga < 1 && list.getCircuit() == TipoLista.Descarga)
            {
                acceptAction(id);
            }
        }
        else
        {
            FacesContext.getCurrentInstance().getExternalContext().redirect("login.xhtml");
        }

         */
    }

        /*
    //se usa
    public void acceptWithConditions(ListData list) throws IOException
    {
        acceptList(list.getListId());

        Condition condition = new Condition();
        condition.setMessage(conditionMessage);
        condition.setListId(list);

        listViewBeanPresenter.guardarCondition(condition);

    }

         */


    /*
    public String getCondiciones(ListData selectedList)
    {
        String res = null;
        if (selectedList != null)
        {
            for (Condition c : listViewBeanPresenter.encontrarTodoCondition())
            {
                if (c.getListId().getListId().equals(selectedList.getListId()))
                {
                    res = c.getMessage();
                }
            }
        }

        return res;
    }
*/

    //se usa
    public void deleteAction(Long id) throws IOException
    {
        listViewBeanPresenter.deleteList(id);

        NavigatorUtil.RedirectToLists();
    }

    /*
    private List<Integer> countMyLists(List<ListData> misListas)
    {
        java.util.List<Integer> res = new ArrayList<>();
        int my_lists_carga = 0;
        int my_lists_descarga = 0;

        for (ListData l : misListas)
        {
            if (l.getCircuit() == TipoLista.Carga)
            {
                my_lists_carga++;
            }
            else if (l.getCircuit() == TipoLista.Descarga)
            {
                my_lists_descarga++;
            }
        }
        res.add(my_lists_carga);
        res.add(my_lists_descarga);


        return res;
    }   */


    //se usa
    public void notAcceptList(Long id) throws IOException
    {
        /*
        java.util.List<Condition> conditions = listViewBeanPresenter.encontrarTodoCondition();
        AcceptList acceptList = listViewBeanPresenter.encontrarPorLista(id);
        if (acceptList != null)
        {
            if (acceptList.getAcceptBool())
            {
                acceptList.setAcceptBool(false);
                listViewBeanPresenter.editaAcceptList(acceptList);
            }
        }
        else
        {
            AcceptList accept = new AcceptList();
            accept.setListId(listViewBeanPresenter.findListById(id));
            accept.setAcceptBool(false);
            listViewBeanPresenter.guardarAcceptList(accept);
        }
        for (Condition c : conditions)
        {
            if (c.getListId().getListId().equals(id))
            {
                listViewBeanPresenter.eliminaCondition(c);
            }
        }

        String url = "my_lists.xhtml";
        FacesContext.getCurrentInstance().getExternalContext().redirect(url);

         */
    }

    //se usa
    public void enviaList() throws IOException
    {
        /*
        if (sendTo == LoginType.Vessel_Operator)
        {
            if (selected_list.getTerminal() != null)
            {
                selected_list.setTerminal(null);
            }
            selected_list.setOperator(sendToPerson);
        }
        else if (sendTo == LoginType.Terminal)
        {
            if (selected_list.getOperator() != null)
            {
                selected_list.setOperator(null);
            }
            selected_list.setTerminal(sendToPerson);
        }
        editAction();

         */
    }

    //se usa
    public void cancela(Long listId) throws IOException
    {
        listViewBeanPresenter.cancelList(listId);

        NavigatorUtil.RedirectToMyLists();
    }

    //se usa
    public boolean checkAddList()
    {
        boolean res = false;
        if ((listViewBeanPresenter.isAnyUserLogged()))
        {
            if (listViewBeanPresenter.hasLoggedUserRole("Container_Operator"))
            {
                res = true;
            }
        }
        return res;
    }

    //se usa
    public boolean checkSendToVesselOperator()
    {
        boolean res = false;
        if (sendTo == LoginType.Vessel_Operator)
        {
            res = true;
        }
        return res;
    }

    public boolean showChangeStopButton()
    {
        return checkEditContainerOperator() && !isViewBeanInEditMode();
    }

    public boolean checkEditContainerOperator()
    {
        boolean result = false;
        if (listViewBeanPresenter.isAnyUserLogged())
        {
            if (listViewBeanPresenter.hasLoggedUserRole("Container_Operator"))
            {
                result = true;
            }
        }

        return result;
    }

    public boolean isViewBeanInEditMode()
    {
        boolean result = false;
        if ((selected_list != null) && (selected_list.getId() != null))
        {
            result = true;
        }
        return result;
    }

    //se usa
    public boolean checkActive()
    {
        List<ListBeanData> listas = getMyAcceptedLists();

        boolean res = false;

        int contadorCarga = 0;
        int contadorDescarga = 0;

        if (listViewBeanPresenter.hasLoggedUserRole("Container_Operator") ||
                listViewBeanPresenter.hasLoggedUserRole("Vessel_Operator") ||
                listViewBeanPresenter.hasLoggedUserRole("TTP") ||
                listViewBeanPresenter.hasLoggedUserRole("Terminal"))
        {
            for (ListBeanData l : listas)
            {
                if (l.getState() != EstadoLista.Rechazada && l.getState() != EstadoLista.Cancelada)
                {
                    if (l.getCircuit() == TipoLista.Carga)
                    {
                        contadorCarga++;
                    }
                    else
                    {
                        contadorDescarga++;
                    }
                }
            }
            if (!(contadorCarga >= 1) || !(contadorDescarga >= 1))
            {
                res = true;
            }
        }
        return res;
    }

    //se usa
    public boolean checkCond(List list)
    {
        boolean res = false;

        /*
        java.util.List<Condition> conditions = listViewBeanPresenter.encontrarTodoCondition();
        for (Condition c : conditions)
        {
            if (c.getListId().equals(list)) {
                res = true;
                break;
            }
        }

         */
        return res;
    }

    //se usa
    public boolean checkEditETD()
    {
        boolean res = false;
        Date currDate = new Date(System.currentTimeMillis());
        if (currDate.compareTo(selectedStop.getStopEtd()) > 0)
        {
            res = true;
        }

        return res;
    }

    //se usa
    public boolean checkFile()
    {
        boolean res = true;
        /*
        for (File f : files)
        {
            if (f.getListId() == listViewBeanPresenter.findListById(selected_list.getId()))
            {
                res = false;
                break;
            }
        }

         */
        return res;
    }

    public void cambiaSelect()
    {
        logger.info("cambiaSelect " + stop_a_buscar);
        selectedStop = findSelectedStop(stop_a_buscar);
        changeListOperatorByDefault(selectedStop);
    }

    private void changeListOperatorByDefault(StopBeanData selectedStop)
    {
        selected_list.setOperator(selectedStop.getAgentName());
    }

    private StopBeanData findSelectedStop(Long stop_a_buscar)
    {
        StopBeanData result = null;
        for (StopBeanData stopBeanData : stops)
        {
            if (stopBeanData.getStopId().equals(stop_a_buscar))
            {
                result = stopBeanData;
                break;
            }
        }
        return result;
    }

    public void cambiaEdit()
    {
        logger.info("cambiaEdit" + stop_a_buscar);
        listViewBeanPresenter.setStopToList(selected_list, stop_a_buscar);
        selectedStop = findSelectedStop(stop_a_buscar);
    }

    public void onToggle(ToggleEvent e)
    {
        visibleColumns[(Integer) e.getData()] = e.getVisibility() == Visibility.VISIBLE;
        listViewBeanPresenter.setVisibleColumns (visibleColumns);
    }
}