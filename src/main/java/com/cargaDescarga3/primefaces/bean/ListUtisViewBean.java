package com.cargaDescarga3.primefaces.bean;

import com.cargaDescarga3.primefaces.enums.EstadoLista;
import com.cargaDescarga3.primefaces.enums.TipoLista;
import com.cargaDescarga3.primefaces.enums.TipoListaConsolidado;
import com.cargaDescarga3.primefaces.model.bean.ListBeanData;
import com.cargaDescarga3.primefaces.model.bean.UtiBeanData;
import com.cargaDescarga3.primefaces.services.IListDataService;
import com.cargaDescarga3.primefaces.services.IUtiDataService;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import javax.faces.annotation.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@Named
@ViewScoped
public class ListUtisViewBean implements Serializable
{
    @ManagedProperty("#{param.listId}")
    private Long listId;

    private String terminal;
    private Integer etd;
    private Integer eta;
    private Date created_date;
    private Date updated_date;
    private String updated_user;
    private Date closing_date;
    private String version;
    private EstadoLista state;
    private TipoLista circuit;
    private TipoListaConsolidado type;
    private String operator;
    private String visit_number;
    private String imo_code;
    private String call_sign;
    private String mmsi;
    private String flag;
    private String port;

    private List<UtiBeanData> utis;
    private List<UtiBeanData> utisFiltered;
    @Autowired
    private IUtiDataService utiDataService;
    @Autowired
    private IListDataService listDataManager;

    private Logger logger = LoggerFactory.getLogger(ListUtisViewBean.class);

    @PostConstruct
    public void init()
    {
        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        listId = Long.parseLong(req.getParameter("listId"));

        logger.info("New ListUtisViewBean for list " + listId);

        ListBeanData list = listDataManager.getListBeanData(listId);

        terminal = list.getTerminal();
        created_date = list.getCreatedDate();
        updated_date = list.getUpdatedDate();
        closing_date = list.getClosingDate();
        version = list.getVersion();
        state = list.getState();
        circuit = list.getCircuit();
        type = list.getType();
        operator = list.getOperator();
        port = list.getPort();

        utis = utiDataService.getUtisForList(listId);
    }
}