package com.cargaDescarga3.primefaces.controller;

import com.cargaDescarga3.primefaces.exceptions.UtiNotFoundException;
import com.cargaDescarga3.primefaces.model.rest.UtiDataRest;
import com.cargaDescarga3.primefaces.services.IListDataService;
import com.cargaDescarga3.primefaces.services.IUserLoggedService;
import com.cargaDescarga3.primefaces.services.IUtiDataService;
import org.apache.commons.lang3.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UtiController
{
    @Autowired
    private IUtiDataService utiDataService;
    @Autowired
    private IListDataService listDataManager;

    @Autowired
    IUserLoggedService userDataManager;

    private Logger logger = LoggerFactory.getLogger(UtiController.class);

    @GetMapping("/api/utis/")
    public List<UtiDataRest> all(Authentication authentication)
    {
        String userLoggedName = userDataManager.getUserLoggedName();
        logger.info("@GetMapping(\"/api/utis/\") By " + authentication.getName() + " or " + userLoggedName);
        for (GrantedAuthority authority : authentication.getAuthorities())
        {
            logger.info("\t" + authority);
        }

        return utiDataService.getAllUtisRest();
    }

    @PostMapping("/api/utis/")
    public UtiDataRest newUti(@RequestBody UtiDataRest newUti)
    {
        return utiDataService.createNewUtiFromUtiDataRest(newUti);
    }

    @GetMapping("/api/utis/{id}")
    public UtiDataRest one(@PathVariable Long id)
    {
        UtiDataRest result = utiDataService.getUtiDataRestFromId(id);
        if (result == null)
        {
            throw new UtiNotFoundException(id);
        }
        return result;
    }

    @PutMapping("/api/utis/{id}")
    UtiDataRest cambiaUti(@RequestBody UtiDataRest newUti, @PathVariable Long id)
    {
        throw new NotImplementedException("To be implemented");
    }
}
