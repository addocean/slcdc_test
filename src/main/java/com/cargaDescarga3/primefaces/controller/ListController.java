package com.cargaDescarga3.primefaces.controller;

import com.cargaDescarga3.primefaces.converter.ListsConverter;
import com.cargaDescarga3.primefaces.exceptions.NoValidStopException;
import com.cargaDescarga3.primefaces.model.data.UserData;
import com.cargaDescarga3.primefaces.model.rest.ListRest;
import com.cargaDescarga3.primefaces.services.IListDataService;
import com.cargaDescarga3.primefaces.services.IStopDataService;
import com.cargaDescarga3.primefaces.services.IUserLoggedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ListController
{
    @Autowired
    private IStopDataService stopDataService;

    @Autowired
    private IListDataService listDataManager;

    @Autowired
    private IUserLoggedService userLoggedDataManager;

    @Autowired
    private ListsConverter myListConverter;

    @GetMapping("/api/lists/")
    public List<ListRest> all(Authentication authentication)
    {
        UserData user = userLoggedDataManager.getAuthenticatedUser(authentication);

        return listDataManager.getMyCreatedListsRest(user);
    }

    @PostMapping("/api/lists/")
    public ListRest newList(@RequestBody ListRest newList, Authentication authentication) throws NoValidStopException
    {
        String principal = (String) authentication.getPrincipal();
        Object details = authentication.getDetails();

        String[] principalArray = principal.split("_");
        int userId = Integer.parseInt(principalArray[principalArray.length - 1]);

        return listDataManager.createNewListFromRestController(newList, userId);
    }

    @GetMapping("/api/lists/{id}")
    public ListRest one(@PathVariable Long id)
    {
        return listDataManager.findListRestById(id);
    }

    @PutMapping("/api/lists/{id}")
    public ListRest cambiaList(
            @RequestBody ListRest newList, @PathVariable Long id)
    {

        return listDataManager.updateListFromRest(newList, id);
    }
}
