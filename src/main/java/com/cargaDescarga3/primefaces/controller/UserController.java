package com.cargaDescarga3.primefaces.controller;

import com.cargaDescarga3.primefaces.model.rest.UserDataRest;
import com.cargaDescarga3.primefaces.services.IUserDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController
{
    @Autowired
    IUserDataService userDataService;

    // TODO: Podríamos devolver directamente el user en vez del token exclusivamente
    @PostMapping("/user/")
    public ResponseEntity<UserDataRest> login(@RequestBody UserDataRest userDataRest)
    {
        ResponseEntity<UserDataRest> responseEntity = null;
        UserDataRest user = userDataService.getUserDataRestLogger(userDataRest.getUserName(), userDataRest.getPassword());
        if (user != null)
        {
            responseEntity = ResponseEntity.accepted().body(user);
        }
        else
        {
            responseEntity = ResponseEntity.notFound().build();
        }

        return responseEntity;
    }
}
