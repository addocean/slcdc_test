package com.cargaDescarga3.primefaces.repository;

import com.cargaDescarga3.primefaces.model.data.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
}
