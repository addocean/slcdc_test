package com.cargaDescarga3.primefaces.repository;

import com.cargaDescarga3.primefaces.model.view.ListView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ListViewRepository extends JpaRepository<ListView, Long>
{
    List<ListView> getByUserId(Long userId);
}

