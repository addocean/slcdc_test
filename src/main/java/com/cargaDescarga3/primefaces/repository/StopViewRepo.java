package com.cargaDescarga3.primefaces.repository;

import com.cargaDescarga3.primefaces.model.view.StopView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StopViewRepo extends JpaRepository<StopView, Long>
{
}
