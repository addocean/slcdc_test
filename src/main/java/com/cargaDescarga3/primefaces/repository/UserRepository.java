package com.cargaDescarga3.primefaces.repository;

import com.cargaDescarga3.primefaces.model.data.UserData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserData, Long>
{
    UserData findByUsername(String userName);
}
