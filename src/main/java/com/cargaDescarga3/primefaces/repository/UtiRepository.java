package com.cargaDescarga3.primefaces.repository;

import com.cargaDescarga3.primefaces.model.data.Uti;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UtiRepository extends JpaRepository<Uti, Long>
{
    List<Uti> getByListId(Long listId);
}
