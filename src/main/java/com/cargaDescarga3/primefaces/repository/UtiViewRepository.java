package com.cargaDescarga3.primefaces.repository;

import com.cargaDescarga3.primefaces.model.view.UtiView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UtiViewRepository extends JpaRepository<UtiView, Long>
{
    List<UtiView> getBylistId(Long listId);
}
