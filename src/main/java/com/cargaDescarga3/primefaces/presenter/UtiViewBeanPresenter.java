package com.cargaDescarga3.primefaces.presenter;

import com.cargaDescarga3.primefaces.model.bean.UtiBeanData;
import com.cargaDescarga3.primefaces.services.IUserLoggedService;
import com.cargaDescarga3.primefaces.services.IUtiDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UtiViewBeanPresenter
{
    @Autowired
    private IUtiDataService utiDataService;

    @Autowired
    private IUserLoggedService loggedDataManager;

    public void createNewUti(UtiBeanData uti_edit)
    {
        utiDataService.createNewUtiFromUtiBean(uti_edit);
    }

    public List<UtiBeanData> getAllUtiBeans()
    {
        return utiDataService.getAllUtisBeans();
    }

    public UtiBeanData getUtiBean(Long id)
    {
        return utiDataService.getUtiBeanDataFromId(id);
    }

    public void saveUtiBean(UtiBeanData utiBeanData)
    {
        utiDataService.editUtiBean(utiBeanData);
    }

    public void removeUtiBean(Long id)
    {
        utiDataService.removeUti(id);
    }

    public boolean isAnyUserLogged()
    {
        return loggedDataManager.isAnyUserLogged();
    }
}
