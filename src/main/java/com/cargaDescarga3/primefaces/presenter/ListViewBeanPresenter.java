package com.cargaDescarga3.primefaces.presenter;

import com.cargaDescarga3.primefaces.model.bean.ListBeanData;
import com.cargaDescarga3.primefaces.model.bean.StopBeanData;
import com.cargaDescarga3.primefaces.model.data.ListData;
import com.cargaDescarga3.primefaces.model.data.Role;
import com.cargaDescarga3.primefaces.model.data.UserData;
import com.cargaDescarga3.primefaces.services.IListDataService;
import com.cargaDescarga3.primefaces.services.IRoleDataService;
import com.cargaDescarga3.primefaces.services.IStopDataService;
import com.cargaDescarga3.primefaces.services.IUserLoggedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;

@Component
public class ListViewBeanPresenter
{
    @Autowired
    private IListDataService listDataManager;
    @Autowired
    private IStopDataService stopDataService;
    @Autowired
    private IRoleDataService roleDataService;
    @Autowired
    private IUserLoggedService userDataManagerSpringSecurity;
    private Boolean[] visibleColumns;

    public boolean isAnyUserLogged()
    {
        return userDataManagerSpringSecurity.isAnyUserLogged();
    }

    public List<ListBeanData> getMyCreatedListsBeans()
    {
        return listDataManager.getMyCreatedListsBeans();
    }

    public boolean hasLoggedUserRole(String role)
    {
        return getRolesFromLoggerUser().contains(findByName(role));
    }

    private Collection<Role> getRolesFromLoggerUser()
    {
        return userDataManagerSpringSecurity.getRolesFromLoggerUser();
    }

    private Role findByName(String container_operator)
    {
        return roleDataService.findByName(container_operator);
    }

    public Long createNewListFromBean(ListBeanData listBeanData, Long stopId)
    {
        UserData user = userDataManagerSpringSecurity.getLoggedUser();
        return listDataManager.createNewListFromBean(listBeanData, stopId, user.getUser_id()).getList_id();
    }

    public String getUserLoggedName()
    {
        return userDataManagerSpringSecurity.getUserLoggedName();
    }

    public void setStopToList(ListBeanData selected_list, Long stop_a_buscar)
    {
        ListData lista = listDataManager.findListById(selected_list.getId());
        lista.setStop_id(stop_a_buscar);
    }

    public ListBeanData createListBeanData(Long id)
    {
        return listDataManager.getListBeanData(id);
    }

    public void deleteList(Long id)
    {
        ListData res = listDataManager.findListById(id);

        listDataManager.eliminaUtis(res);

        listDataManager.elimina(res);
    }

    public void cancelList(Long list_id)
    {
        ListData lista = listDataManager.findListById(list_id);
        if (isAnyUserLogged())
        {
            if (hasLoggedUserRole("Vessel_Operator"))
            {
                lista.setOperator(null);
            }
            if (hasLoggedUserRole("Terminal"))
            {
                lista.setTerminal(null);
            }
            listDataManager.edita(lista);
        }
    }

    public List<StopBeanData> getAllStops()
    {
        return stopDataService.getAllStops();
    }

    public Long createNewListFromBean(ListBeanData selected_list, long stopId)
    {
        UserData user = userDataManagerSpringSecurity.getLoggedUser();
        ListData listData = listDataManager.createNewListFromBean(selected_list, stopId, user.getUser_id());
        Long result = listData.getList_id();

        return result;
    }

    public Boolean[] getVisibleColumns()
    {
        if (visibleColumns == null)
        {
            visibleColumns = new Boolean[15];
            for (int i = 0; i < 15; i++)
            {
                visibleColumns[i] = true;
            }
        }
        return visibleColumns;
    }

    public void setVisibleColumns(Boolean[] visibleColumns)
    {
        this.visibleColumns = visibleColumns;
    }
}
