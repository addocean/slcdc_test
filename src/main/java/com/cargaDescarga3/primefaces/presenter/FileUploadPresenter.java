package com.cargaDescarga3.primefaces.presenter;

import com.cargaDescarga3.primefaces.model.bean.UtiBeanData;
import com.cargaDescarga3.primefaces.services.UtiDataService;
import com.cargaDescarga3.primefaces.utils.NavigatorUtil;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

@Component
public class FileUploadPresenter
{
    @Autowired
    UtiDataService utiDataService;

    public void fileUploaded(UploadedFile file, long listId)
    {
        try
        {
            BufferedReader reader = new BufferedReader(new InputStreamReader(file.getInputstream()));
            String line;
            while ((line = reader.readLine())!= null)
            {
                String[] elements = line.split(";");
                UtiBeanData utiBeanData = new UtiBeanData();
                utiBeanData.setListId(listId);
                utiBeanData.setPlate(elements[0]);
                utiDataService.createNewUtiFromUtiBean(utiBeanData);
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        try
        {
            NavigatorUtil.RedirectToListDetails(listId);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
