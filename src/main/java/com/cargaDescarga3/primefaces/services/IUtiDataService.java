package com.cargaDescarga3.primefaces.services;

import com.cargaDescarga3.primefaces.model.bean.UtiBeanData;
import com.cargaDescarga3.primefaces.model.rest.UtiDataRest;

import java.util.List;

public interface IUtiDataService
{
    List<UtiBeanData> getUtisForList(Long listId);

    List<UtiDataRest> getAllUtisRest();

    UtiDataRest createNewUtiFromUtiDataRest(UtiDataRest newUti);

    UtiDataRest getUtiDataRestFromId(Long id);

    Long createNewUtiFromUtiBean(UtiBeanData utiBeanData);

    List<UtiBeanData> getAllUtisBeans();

    UtiBeanData getUtiBeanDataFromId(Long id);

    void removeUti(Long id);

    void editUtiBean(UtiBeanData utiBeanData);

    void removeUtisFromListId(Long list_id);
}
