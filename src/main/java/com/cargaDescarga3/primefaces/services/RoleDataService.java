package com.cargaDescarga3.primefaces.services;

import com.cargaDescarga3.primefaces.model.data.Role;
import com.cargaDescarga3.primefaces.repository.RoleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
class RoleDataService implements IRoleDataService
{
    @Autowired
    private RoleRepository roleRepository;

    private Logger logger = LoggerFactory.getLogger(RoleDataService.class);

    @PostConstruct
    public void init()
    {
        logger.info("Creating Service RoleDataService...");
    }

    public List<Role> encontrarTodo()
    {
        return roleRepository.findAll();
    }

    public Role encontrar(Long id)
    {
        return roleRepository.getOne(id);
    }

    public void guardar(Role role)
    {
        roleRepository.save(role);
    }

    public void edita(Role newRole)
    {
        roleRepository.findById(newRole.getRole_id()).map(role ->
        {
            role.setRole(newRole.getRole());

            return roleRepository.save(role);
        }).orElseGet(() ->
        {
            newRole.setRole_id(newRole.getRole_id());
            return roleRepository.save(newRole);
        });
    }

    public Role findByName(String role)
    {
        Role res = null;
        for (Role r : roleRepository.findAll())
        {
            if (r.getRole().toString().equals(role))
            {
                res = r;
            }
        }
        return res;
    }
}
