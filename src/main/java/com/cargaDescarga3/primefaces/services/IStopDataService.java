package com.cargaDescarga3.primefaces.services;

import com.cargaDescarga3.primefaces.exceptions.NoValidStopException;
import com.cargaDescarga3.primefaces.model.bean.StopBeanData;

import java.util.List;

public interface IStopDataService
{
    List<StopBeanData> getAllStops();

    StopBeanData getStopBeanFromId(long stopId) throws NoValidStopException;
}
