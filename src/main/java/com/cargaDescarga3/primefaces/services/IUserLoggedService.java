package com.cargaDescarga3.primefaces.services;

import com.cargaDescarga3.primefaces.model.bean.UserBeanData;
import com.cargaDescarga3.primefaces.model.data.Role;
import com.cargaDescarga3.primefaces.model.data.UserData;
import org.springframework.security.core.Authentication;

import java.util.Collection;

public interface IUserLoggedService
{
    boolean isAnyUserLogged();

    Collection<Role> getRolesFromLoggerUser();

    String getUserLoggedName();

    UserData getLoggedUser();

    UserData getAuthenticatedUser(Authentication authentication);

    UserBeanData getUserBeanData(UserData user);

    UserBeanData getUserBeanDataFromId(Long userId);

    UserBeanData getLoggerUserBeanData();

    UserData getUserFromId(long userThatCreatesId);
}
