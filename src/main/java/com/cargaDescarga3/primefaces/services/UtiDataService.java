package com.cargaDescarga3.primefaces.services;

import com.cargaDescarga3.primefaces.converter.UtisConverter;
import com.cargaDescarga3.primefaces.model.bean.UtiBeanData;
import com.cargaDescarga3.primefaces.model.data.ListData;
import com.cargaDescarga3.primefaces.model.data.Uti;
import com.cargaDescarga3.primefaces.model.rest.UtiDataRest;
import com.cargaDescarga3.primefaces.model.view.UtiView;
import com.cargaDescarga3.primefaces.repository.UtiRepository;
import com.cargaDescarga3.primefaces.repository.UtiViewRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service
public class UtiDataService implements IUtiDataService
{
    @Autowired
    private UtiRepository utiRepository;

    @Autowired
    private UtiViewRepository utiViewRepository;

    @Autowired
    private UtisConverter utisConverter;

    @Autowired
    private IListDataService listDataService;

    private Logger logger = LoggerFactory.getLogger(UtiDataService.class);

    @PostConstruct
    public void init()
    {
        logger.info("Creating Service UtiDataService...");
    }

    @Override
    public List<UtiBeanData> getUtisForList(Long listId)
    {
        List<UtiBeanData> result = new ArrayList<>();

        List<UtiView> utisData = utiViewRepository.getBylistId(listId);
        for (UtiView uti : utisData)
        {
            UtiBeanData utiBeanData = utisConverter.convertUtiViewToUtiBean(uti);
            result.add(utiBeanData);
        }
        return result;
    }

    @Override
    public List<UtiDataRest> getAllUtisRest()
    {
        List<Uti> utis = utiRepository.findAll();
        return utisConverter.convertListUtiDataToListUtiDataRest(utis);
    }

    @Override
    public UtiDataRest createNewUtiFromUtiDataRest(UtiDataRest newUti)
    {
        ListData utiList = listDataService.getOne(newUti.getListId());

        Uti uti = utisConverter.convertUtiDataRestToUtiData(newUti);
        uti.setListId(utiList);
        utiRepository.save(uti);

        /*
        String errores = Uti.checkValidez(uti);
        errores += Uti.checkValidezConDemas(saved, utis);

        if (!errores.equals(""))
        {
            throw new SLCDCGeneralException(errores);
        }
        else
        {
            saved = utiDataService.saveListData(saved);
        }
         */

        return utisConverter.convertUtiDataToUtiDataRest(uti);
    }

    @Override
    public UtiDataRest getUtiDataRestFromId(Long id)
    {
        Uti uti = utiRepository.getOne(id);
        return utisConverter.convertUtiDataToUtiDataRest(uti);
    }

    @Override
    public Long createNewUtiFromUtiBean(UtiBeanData utiBeanData)
    {
        ListData listData = listDataService.getOne(utiBeanData.getListId());
        Uti uti = utisConverter.convertUtiBeanDataToUtiData(utiBeanData, listData);
        utiRepository.save(uti);
        return uti.getUti_id();
    }

    @Override
    public List<UtiBeanData> getAllUtisBeans()
    {
        List<UtiView> utis = utiViewRepository.findAll();
        return utisConverter.convertListUtiDataToListUtiBean(utis);
    }

    @Override
    public UtiBeanData getUtiBeanDataFromId(Long id)
    {
        UtiView utiView = utiViewRepository.getOne(id);
        return utisConverter.convertUtiViewToUtiBean(utiView);
    }

    @Override
    public void removeUti(Long id)
    {
        utiRepository.deleteById(id);
    }

    @Override
    public void editUtiBean(UtiBeanData utiBeanData)
    {
        ListData listData = listDataService.getOne(utiBeanData.getListId());
        Uti uti = utisConverter.convertUtiBeanDataToUtiData(utiBeanData, listData);
        utiRepository.save(uti);
    }

    @Override
    public void removeUtisFromListId(Long list_id)
    {
        List<Uti> utis = utiRepository.getByListId(list_id);
        for (Uti uti : utis)
        {
            utiRepository.delete(uti);
        }
    }
}
