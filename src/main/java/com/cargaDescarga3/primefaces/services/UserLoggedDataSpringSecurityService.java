package com.cargaDescarga3.primefaces.services;

import com.cargaDescarga3.primefaces.converter.UsersConverter;
import com.cargaDescarga3.primefaces.model.bean.UserBeanData;
import com.cargaDescarga3.primefaces.model.data.Role;
import com.cargaDescarga3.primefaces.model.data.UserData;
import com.cargaDescarga3.primefaces.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
@Primary
class UserLoggedDataSpringSecurityService implements IUserLoggedService
{
    @Autowired
    private IRoleDataService roleDataService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UsersConverter userConverter;

    @Override
    public boolean isAnyUserLogged()
    {
        return SecurityContextHolder.getContext().getAuthentication().getPrincipal() != null;
    }

    @Override
    public Collection<Role> getRolesFromLoggerUser()
    {
        Collection<Role> result = new ArrayList<>();
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails)
        {
            for (GrantedAuthority authority : ((UserDetails) principal).getAuthorities())
            {
                Role role = roleDataService.findByName(authority.getAuthority());
                result.add(role);
            }
        }
        return result;
    }

    @Override
    public String getUserLoggedName()
    {
        String userName;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails)
        {
            userName = ((UserDetails) principal).getUsername();
        }
        else
        {
            userName = principal.toString();
        }
        return userName;
    }

    @Override
    public UserData getLoggedUser()
    {
        String userName = getUserLoggedName();
        return userRepository.findByUsername(userName);
    }

    @Override
    public UserData getAuthenticatedUser(Authentication authentication)
    {
        UserData result = null;
        if (authentication != null)
        {
            Object principal = authentication.getPrincipal();
            if (principal instanceof UserData)
            {
                result = (UserData) principal;
            }
            else if (principal instanceof String)
            {
                String[] principalSplitted = ((String) principal).split("_");
                if (principalSplitted.length >= 2)
                {
                    result = userRepository.getOne(Long.parseLong(principalSplitted[principalSplitted.length - 1]));
                }
            }
        }
        return result;
    }

    @Override
    public UserBeanData getUserBeanData(UserData user)
    {
        UserBeanData userBeanData = userConverter.convertFromUserDataToUserBeanData(user);
        return userBeanData;
    }

    @Override
    public UserBeanData getUserBeanDataFromId(Long userId)
    {
        UserData user = userRepository.getOne(userId);
        return getUserBeanData(user);
    }

    @Override
    public UserBeanData getLoggerUserBeanData()
    {
        return getUserBeanData(getLoggedUser());
    }

    @Override
    public UserData getUserFromId(long userThatCreatesId)
    {
        return userRepository.getOne(userThatCreatesId);
    }
}
