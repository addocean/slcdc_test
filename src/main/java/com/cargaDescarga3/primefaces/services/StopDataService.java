package com.cargaDescarga3.primefaces.services;

import com.cargaDescarga3.primefaces.converter.ListsConverter;
import com.cargaDescarga3.primefaces.converter.StopsConverter;
import com.cargaDescarga3.primefaces.exceptions.NoValidStopException;
import com.cargaDescarga3.primefaces.model.bean.StopBeanData;
import com.cargaDescarga3.primefaces.model.view.StopView;
import com.cargaDescarga3.primefaces.repository.StopViewRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
public class StopDataService implements IStopDataService
{

    @Autowired
    private StopViewRepo stopViewRepo;

    @Autowired
    private ListsConverter listsConverter;

    @Autowired
    private StopsConverter stopsConverter;

    private Logger logger = LoggerFactory.getLogger(StopDataService.class);

    @PostConstruct
    public void init()
    {
        logger.info("Creating Service StopDataService...");
    }

    @Override
    public List<StopBeanData> getAllStops()
    {
        List<StopView> stopsView = stopViewRepo.findAll();
        return stopsConverter.convertListsStop(stopsView);
    }

    @Override
    public StopBeanData getStopBeanFromId(long stopId) throws NoValidStopException
    {
        StopView stopView = stopViewRepo.getOne(stopId);

        if (stopView == null)
        {
            throw new NoValidStopException();
        }

        StopBeanData stopBeanData = stopsConverter.convertStopBeanFromStopView(stopView);
        return stopBeanData;
    }
}
