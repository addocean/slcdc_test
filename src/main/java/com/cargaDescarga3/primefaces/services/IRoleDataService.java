package com.cargaDescarga3.primefaces.services;

import com.cargaDescarga3.primefaces.model.data.Role;

import java.util.List;

public interface IRoleDataService
{
    List<Role> encontrarTodo();

    Role encontrar(Long id);

    void guardar(Role role);

    void edita(Role newRole);

    Role findByName(String role);
}
