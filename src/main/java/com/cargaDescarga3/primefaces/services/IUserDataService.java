package com.cargaDescarga3.primefaces.services;

import com.cargaDescarga3.primefaces.model.data.UserData;
import com.cargaDescarga3.primefaces.model.rest.UserDataRest;

public interface IUserDataService
{
    UserDataRest getUserDataRestLogger(String username, String pwd);

    UserData getLoggedUser();
}
