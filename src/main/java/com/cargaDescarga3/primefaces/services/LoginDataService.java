package com.cargaDescarga3.primefaces.services;

import com.cargaDescarga3.primefaces.model.data.UserData;
import com.cargaDescarga3.primefaces.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
class LoginDataService implements ILoginDataService
{
    @Autowired
    private UserRepository userRepository;

    private Logger logger = LoggerFactory.getLogger(LoginDataService.class);

    @PostConstruct
    public void init()
    {
        logger.info("Creating Service LoginDataService...");
    }

    @Override
    public UserData iniciaSesion(String username, String password)
    {
        UserData res = null;
        if (username != null && password != null)
        {
            // TODO: Proteger si el repo no encuentra el usuario con ese nombre.
            UserData foundUser = userRepository.findByUsername(username);
            if (foundUser.getPassword().equals(password))
            {
                res = foundUser;
            }
            // Dar mensaje de error si se introduce mal la contraseña y otro distinto si no existe el usuario (está en el ViewBean)
        }
        return res;
    }

    @Override
    public String guardar(UserData newUser)
    {
        int count = 0;
        UserData foundUser = userRepository.findByUsername(newUser.getUsername());
        if (foundUser != null)
        {
            count++;
        }
        if (count == 0)
        {
            userRepository.save(newUser);
            return "UserData added. ";
        }
        else
        {
            return "UserData not added. Try again.";
        }
    }

    @Override
    public UserData getUserByName(String userName)
    {
        List<UserData> users = userRepository.findAll();
        UserData res = null;
        for (UserData u : users)
        {
            if (u.getUsername().equals(userName))
            {
                res = u;
            }
        }
        return res;
    }
}
