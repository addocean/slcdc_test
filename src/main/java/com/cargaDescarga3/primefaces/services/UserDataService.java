package com.cargaDescarga3.primefaces.services;

import com.cargaDescarga3.primefaces.model.data.UserData;
import com.cargaDescarga3.primefaces.model.rest.UserDataRest;
import com.cargaDescarga3.primefaces.repository.UserRepository;
import com.cargaDescarga3.primefaces.security.JWTUtils;
import com.cargaDescarga3.primefaces.security.SpringSecurityConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
class UserDataService implements IUserDataService
{
    @Autowired
    UserRepository userRepository;

    @Autowired
    JWTUtils jwtUtils;

    @Autowired
    SpringSecurityConfig springSecurityConfig;

    private Logger logger = LoggerFactory.getLogger(UserDataService.class);

    @PostConstruct
    public void init()
    {
        logger.info("Creating Service UserDataService...");
    }

    @Override
    public UserDataRest getUserDataRestLogger(String username, String pwd)
    {
        UserDataRest result = null;
        UserData user = userRepository.findByUsername(username);
        if (user != null)
        {
            if (springSecurityConfig.passwordEncoder().matches(pwd, user.getPassword()))
            {
                result = new UserDataRest();
                result.setUserName(username);
                result.setPassword("****");

                String token = jwtUtils.getTokenForUser(user);
                result.setToken(token);
            }
        }
        return result;
    }

    @Override
    public UserData getLoggedUser()
    {
        return (UserData) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}