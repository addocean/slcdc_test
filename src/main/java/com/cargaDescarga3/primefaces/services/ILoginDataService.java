package com.cargaDescarga3.primefaces.services;

import com.cargaDescarga3.primefaces.model.data.UserData;

public interface ILoginDataService
{
    UserData iniciaSesion(String username, String password);

    String guardar(UserData newUser);

    UserData getUserByName(String userName);
}
