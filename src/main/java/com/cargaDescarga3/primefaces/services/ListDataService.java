package com.cargaDescarga3.primefaces.services;

import com.cargaDescarga3.primefaces.converter.ListsConverter;
import com.cargaDescarga3.primefaces.enums.EstadoLista;
import com.cargaDescarga3.primefaces.exceptions.NoValidStopException;
import com.cargaDescarga3.primefaces.model.bean.ListBeanData;
import com.cargaDescarga3.primefaces.model.bean.StopBeanData;
import com.cargaDescarga3.primefaces.model.bean.UserBeanData;
import com.cargaDescarga3.primefaces.model.data.ListData;
import com.cargaDescarga3.primefaces.model.data.UserData;
import com.cargaDescarga3.primefaces.model.rest.ListRest;
import com.cargaDescarga3.primefaces.model.view.ListView;
import com.cargaDescarga3.primefaces.repository.ListRepository;
import com.cargaDescarga3.primefaces.repository.ListViewRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.List;

@Service
public class ListDataService implements IListDataService
{
    @Autowired
    private ListRepository listRepository;

    @Autowired
    private ListViewRepository listViewRepository;

    @Autowired
    private StopDataService stopDataService;

    @Autowired
    private UtiDataService utiDataService;

    @Autowired
    private IUserLoggedService userDataService;

    @Autowired
    private ListsConverter listsConverter;

    private Logger logger = LoggerFactory.getLogger(ListDataService.class);

    @PostConstruct
    public void init()
    {
        logger.info("Creating Service ListDataService...");
    }

    @Override
    public List<ListBeanData> getMyCreatedListsBeans()
    {
        UserData loggedUser = userDataService.getLoggedUser();
        List<ListView> myLists = listViewRepository.getByUserId(loggedUser.getUser_id());

        List<ListBeanData> myListsBeanData = listsConverter.convertListViewListToListBeanDataList(myLists);

        return myListsBeanData;
    }

    @Override
    public List<ListRest> getMyCreatedListsRest(UserData user)
    {
        List<ListData> myLists = listRepository.getByCreateduser(user);
        return listsConverter.convertListDataListToListRestList(myLists);
    }

    @Override
    public ListData findListById(Long id)
    {
        return listRepository.getOne(id);
    }

    @Override
    public void elimina(ListData list)
    {
        /*
        Report rep = null;
        List<Report> allRep = reportRepository.findAll();
        for (Report r : allRep)
        {
            if (r.getListId().getListId().equals(list.getListId()))
            {
                rep = r;
                break;
            }
        }
        if (rep != null)
        {
            reportRepository.delete(rep);
        }

        ListTransmission listTransmission = null;
        List<ListTransmission> allListTransmission = listTransmissionRepository.findAll();
        for (ListTransmission l : allListTransmission)
        {
            if (l.getListId().getListId().equals(list.getListId()))
            {
                listTransmission = l;
                break;
            }
        }
        if (listTransmission != null)
        {
            listTransmissionRepository.delete(listTransmission);
        }
        */

        listRepository.delete(list);
    }

    @Override
    public void eliminaUtis(ListData list)
    {
        utiDataService.removeUtisFromListId(list.getList_id());
    }

    @Override
    public void edita(ListData newList)
    {
        listRepository.findById(newList.getList_id()).map(list ->
        {
            list.setCircuit(newList.getCircuit());
            list.setType(newList.getType());
            list.setOperator(newList.getOperator());
            list.setPort(newList.getPort());
            list.setTerminal(newList.getTerminal());
            list.setUpdated_date(new Date(System.currentTimeMillis()));
            list.setVersion(newList.getVersion());
            list.setState(newList.getState());
            list.setStop_id(newList.getStop_id());

            return listRepository.save(list);
        }).orElseGet(() ->
        {
            newList.setList_id(newList.getList_id());
            return listRepository.save(newList);
        });
    }

    @Override
    public ListRest createNewListFromRestController(ListRest newList, long userThatCreatesId) throws NoValidStopException
    {
        ListData listData = listsConverter.converListRestToListData(newList);

        long listId = createNewListFromData(listData, newList.getStopId(), userThatCreatesId).getList_id();
        ListView listView = listViewRepository.getOne(listId);

        return listsConverter.convertListViewToListRest(listView);
    }

    @Override
    public ListBeanData getListBeanData(Long id)
    {
        ListView listView = listViewRepository.getOne(id);
        ListBeanData listBeanData = listsConverter.convertListViewToListBeanData(listView);
        return listBeanData;
    }

    @Override
    public ListData createNewListFromBean(ListBeanData listBeanData, long stopId, Long userId)
    {
        ListData listData = null;
        try
        {
            StopBeanData stopBean = stopDataService.getStopBeanFromId(stopId);
            UserBeanData userBean = userDataService.getUserBeanDataFromId(userId);
            listBeanData.setStopBeanData(stopBean);
            listBeanData.setUserBeanData(userBean);
            listData = listsConverter.convertListBeanDataToListData(listBeanData);

            listData = createNewListFromData(listData, stopId, userId);
        }
        catch (NoValidStopException e)
        {
            e.printStackTrace();
        }
        return listData;
    }

    @Override
    public ListRest findListRestById(Long id)
    {
        ListView listView = listViewRepository.getOne(id);
        ListRest listRest = listsConverter.convertListViewToListRest(listView);
        return listRest;
    }

    private ListData createNewListFromData(ListData listData, Long stopId, long userThatCreatesId) throws NoValidStopException
    {
        listData.setCreated_date(new Date(System.currentTimeMillis()));
        listData.setUpdated_date(new Date(System.currentTimeMillis()));

        if (stopDataService.getStopBeanFromId(stopId) != null)
        {
            listData.setStop_id(stopId);

            UserData userThatCreates = userDataService.getUserFromId(userThatCreatesId);
            listData.setCreateduser(userThatCreates);

            // El usuario último que actualiza la lista debe ser este mismo que la crea, pero no el nombre solo

            listData.setState(EstadoLista.Pendiente);

            listData = listRepository.save(listData);
        }

        return listData;
    }

    @Override
    public ListData getOne(Long listId)
    {
        return listRepository.getOne(listId);
    }

    @Override
    public ListRest updateListFromRest(ListRest newList, Long id)
    {
        ListData list = findListById(id);
        if (list == null)
        {
            list = new ListData();
            list.setCreated_date(new Date(System.currentTimeMillis()));
        }
        list.setCircuit(newList.getCircuit());
        list.setType(newList.getType());
        list.setOperator(newList.getOperator());
        list.setPort(newList.getPort());
        list.setTerminal(newList.getTerminal());
        list.setUpdated_date(new Date(System.currentTimeMillis()));
        list.setClosing_date(newList.getClosing_date());
        list.setVersion(newList.getVersion());
        list.setState(newList.getState());
        list.setCreateduser(userDataService.getLoggedUser());
        list.setStop_id(newList.getStopId());

        listRepository.save(list);

        ListView listView = listViewRepository.getOne(id);
        return listsConverter.convertListViewToListRest(listView);
    }
}
