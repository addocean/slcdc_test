package com.cargaDescarga3.primefaces.services;

import com.cargaDescarga3.primefaces.exceptions.NoValidStopException;
import com.cargaDescarga3.primefaces.model.bean.ListBeanData;
import com.cargaDescarga3.primefaces.model.data.ListData;
import com.cargaDescarga3.primefaces.model.data.UserData;
import com.cargaDescarga3.primefaces.model.rest.ListRest;

import java.util.List;

public interface IListDataService
{
    List<ListBeanData> getMyCreatedListsBeans();

    List<ListRest> getMyCreatedListsRest(UserData user);

    ListData findListById(Long id);

    void elimina(ListData list);

    void eliminaUtis(ListData list);

    void edita(ListData newList);

    ListRest createNewListFromRestController(ListRest newList, long userThatCreatesId) throws NoValidStopException;

    ListBeanData getListBeanData(Long id);

    ListData createNewListFromBean(ListBeanData selected_list, long stopId, Long user_id);

    ListRest findListRestById(Long id);

    ListData getOne(Long listId);

    ListRest updateListFromRest(ListRest newList, Long id);
}
