package com.cargaDescarga3.primefaces.model.data;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity (name = "user_data")
public class UserData
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long user_id;

    private String username;
    private String password;

    @ManyToMany(fetch=FetchType.EAGER)
    @JoinTable(name = "Users_Role_Detail")
    private Set<Role> role_id;
}
