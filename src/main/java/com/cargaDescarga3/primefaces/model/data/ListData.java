package com.cargaDescarga3.primefaces.model.data;

import com.cargaDescarga3.primefaces.enums.EstadoLista;
import com.cargaDescarga3.primefaces.enums.TipoLista;
import com.cargaDescarga3.primefaces.enums.TipoListaConsolidado;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity(name = "List")
public class ListData
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long list_id;

    private TipoLista circuit;
    private TipoListaConsolidado type;
    private String operator;
    private String port;
    private String terminal;
    private Date created_date;
    private Date updated_date;
    private Date closing_date;
    private String version;
    private EstadoLista state;

    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne
    @JoinColumn(name = "created_user")
    private UserData createduser;

    @JoinColumn(name = "stop_id")
    private Long stop_id;
}




























