package com.cargaDescarga3.primefaces.model.data;

import com.cargaDescarga3.primefaces.enums.*;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity(name = "Uti")
public class Uti
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long uti_id;

    @ManyToOne
    @JoinColumn(name = "listId")
    private ListData listId;

    private TipoTransporte type;
    private Integer iso6364_code;
    private Afirmacion iso6364_compliance;
    private Float length;
    private Float height;
    private Float width;
    private String plate;
    private EstadoUti state;
    private CircuitoUti circuit;
    private String booking;
    private String bill_of_lading;
    private String port;
    private String nextPort;
    private Float gross_weight;
    private Float verified_weight;
    private Integer extra_height;
    private Integer extra_left_width;
    private Integer extra_right_width;
    private Integer extra_front_height;
    private Integer extra_back_length;
    private String goods_description;
    private String goods_remarks;
    private String instructions;
    private String instructions_remarks;
    private String shipping_company;
    private String shipping_service;
    private Afirmacion soc;
    private String operator;
    private Integer transshipment_call;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "refeer_id")
    private RefeerData refeer_id;

    public static String checkValidez(Uti uti){

        String todo = "";
        if (uti.getIso6364_compliance() == Afirmacion.Si && uti.getIso6364_code() == null)
        {
            todo += "Has puesto el compliance del ISO como 'Si', pero no has puesto contenido\n";
        }
        if (uti.getIso6364_compliance() == Afirmacion.No && uti.getIso6364_code() != null)
        {
            todo += "Has puesto el compliance del ISO como 'No', pero has puesto el código\n";
        }
        if (uti.getLength() == null || uti.getHeight() == null || uti.getWidth() == null)
        {
            if (uti.getType() == null && uti.getIso6364_code() == null)
            {
                todo += "El código del ISO está sin completar, además del tipo. A parte, la longitud, altura o anchura están vacíos\n";
            }
        }
        if (uti.getBooking() == null && uti.getCircuit() == CircuitoUti.exportacion)
        {
            todo += "En un paquete de envío de exportación no has puesto booking\n";
        }
        if (uti.getBill_of_lading() == null)
        {
            if (uti.getListId().getCircuit() == TipoLista.Descarga)
            {
                todo += "El Bill of Lading es nulo y la lista es de descarga.\n";
            }
            if (uti.getListId().getCircuit() == TipoLista.Carga
                    && (uti.getCircuit() == CircuitoUti.Transbordo_entre_terminales)
                    || (uti.getCircuit() == CircuitoUti.Transbordo_misma_terminal))
            {
                todo += "Bill of Lading nulo y Lista de carga, y una de las dos posibilidades: trasbordo entre terminales o en la misma terminal\n";
            }
        }
        if (uti.getNextPort() == null)
        {
            if (uti.getListId().getCircuit() == TipoLista.Carga)
            {
                todo += "En una lista de carga hay que poner el siguiente puerto.\n";
            }
        }
        if (uti.getInstructions() != null && uti.getInstructions_remarks() == null)
        {
            todo += "No has puestos detalles a las instrucciones.\n";
        }
        if (uti.getTransshipment_call() == null
                && (uti.getCircuit() == CircuitoUti.Transbordo_misma_terminal
                || uti.getCircuit() == CircuitoUti.Transbordo_entre_terminales))
        {
            todo += "trans shipment call nulo y trasbordo en la misma terminal o entre terminales.\n";
        }
        return todo;
    }

    public static String checkValidezConDemas(Uti uti, java.util.List<Uti> utis)
    {
        int contador = 0;
        String errores = "";
        for (Uti u : utis)
        {
            if (uti.getListId() == u.getListId())
            {
                if ((uti.getPlate() != null) && uti.getPlate().equals(u.getPlate()))
                {
                    contador++;
                }
            }
        }

        if (contador > 0)
        {
            errores += "No puedes hacer utis duplicados\n";
        }
        return errores;
    }
}
