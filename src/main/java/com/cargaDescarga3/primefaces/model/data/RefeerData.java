package com.cargaDescarga3.primefaces.model.data;

import com.cargaDescarga3.primefaces.enums.Afirmacion;
import com.cargaDescarga3.primefaces.enums.Apertura;
import com.cargaDescarga3.primefaces.enums.Temperatura;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity(name = "Refeer")
public class RefeerData
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long refeer_id;
    private Afirmacion active;
    private Afirmacion terminal_connected;
    private Afirmacion ship_connected;
    private Float transport_temperature;
    private Float min_temperature;
    private Float max_temperature;
    private Temperatura um_temperature;
    private Afirmacion diesel_generator;
    private Apertura vent_grilles_state;
    private Float vent_grilles_opening;
    private Float air_flow;
    private Afirmacion humidifier;
    private Float rel_air_humidity;
    private Afirmacion atmosphere_control;
    private Float oxygen_level;
    private Float nitrogen_level;
    private Float carbon_dioxide_level;
    private String instructions;

    public static String checkValidez(RefeerData refeer){

        String todo = "";
        if (refeer.getActive() == null)
        {
            todo += "refeer no tiene valor de activación. Ponga uno.\n";
        }

        if (refeer.getActive() == Afirmacion.Si)
        {
            if (refeer.getTerminal_connected() != Afirmacion.Si)
            {
                todo += "RefeerData activa, pero no conectada con una terminal\n";
            }
            if (refeer.getShip_connected() != Afirmacion.Si)
            {
                todo += "RefeerData activa, pero sin conexion a barco\n";
            }
            if (refeer.getTransport_temperature() == null)
            {
                todo += "RefeerData activa, pero sin temperatura de transporte\n";
            }
            if (refeer.getMin_temperature() == null)
            {
                todo += "RefeerData activa, pero sin temperatura mínima\n";
            }
            if (refeer.getMax_temperature() == null)
            {
                todo += "RefeerData activa, pero sin temperatura máxima\n";
            }
            if (refeer.getDiesel_generator() == null)
            {
                todo += "RefeerData activa, pero sin valor de generador diesel\n";
            }
            if (refeer.getVent_grilles_state() == null)
            {
                todo += "RefeerData activa, pero sin valor para el estado de las rejillas de ventilacion\n";
            }
            if (refeer.getAir_flow() == null)
            {
                todo += "RefeerData activa, pero sin valor para el flujo del aire\n";
            }
            if (refeer.getAtmosphere_control() == null)
            {
                todo += "RefeerData activa, pero no hay valor para el control de atmosfera\n";
            }
            if (refeer.getInstructions() == null)
            {
                todo += "RefeerData activa, pero sin instrucciones\n";
            }
        }

        if (refeer.getHumidifier() == Afirmacion.Si && refeer.getRel_air_humidity() == null)
        {
            todo += "RefeerData con humidificador, pero sin valor para la humedad del refeer\n";
        }

        if (refeer.getAtmosphere_control() == Afirmacion.Si)
        {
            if (refeer.getOxygen_level() == null)
            {
                todo += "RefeerData con control de atmosfera, pero sin valor de nivel de oxigeno\n";
            }
            if (refeer.getNitrogen_level() == null)
            {
                todo += "RefeerData con control de atmosfera, pero sin nivel de nitrogeno\n";
            }
            if (refeer.getCarbon_dioxide_level() == null)
            {
                todo += "RefeerData con control de atmosfera, pero sin nivel de dioxido de carbono\n";
            }
        }

        if (refeer.getVent_grilles_state() == Apertura.Abiertas && refeer.getVent_grilles_opening() == null)
        {
            todo += "RefeerData con rejillas abiertas, pero sin valor de nivel de apertura de las rejillas\n";
        }

        if (refeer.getTransport_temperature() != null || refeer.getMin_temperature() != null || refeer.getMax_temperature() != null)
        {
            if (refeer.getUm_temperature() == null)
            {
                todo += "RefeerData con algún valor de temperatura, pero sin valor para el tipo de métrica de temperatura\n";
            }
        }
        return todo;
    }
}
