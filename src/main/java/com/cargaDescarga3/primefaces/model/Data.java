package com.cargaDescarga3.primefaces.model;


import com.cargaDescarga3.primefaces.enums.*;

import javax.annotation.ManagedBean;
import javax.enterprise.context.ApplicationScoped;

@ManagedBean
@ApplicationScoped
public class Data
{
    public Afirmacion[] getAfirmaciones()
    {
        return Afirmacion.values();
    }

    public Apertura[] getAperturas()
    {
        return Apertura.values();
    }

    public CircuitoUti[] getCircuitosUti()
    {
        return CircuitoUti.values();
    }

    public EstadoInforme[] getEstadosInforme()
    {
        return EstadoInforme.values();
    }

    public EstadoLista[] getEstadosLista()
    {
        return EstadoLista.values();
    }

    public EstadoUti[] getEstadosUti()
    {
        return EstadoUti.values();
    }

    public GrupoSustancia[] getGruposSustancia()
    {
        return GrupoSustancia.values();
    }

    public Temperatura[] getTemperaturas()
    {
        return Temperatura.values();
    }

    public TipoLista[] getTiposLista()
    {
        return TipoLista.values();
    }

    public TipoListaConsolidado[] getTiposListaConsolidado()
    {
        return TipoListaConsolidado.values();
    }

    public TipoPrecinto[] getTiposPrecinto()
    {
        return TipoPrecinto.values();
    }

    public TipoTransporte[] getTiposTransporte()
    {
        return TipoTransporte.values();
    }

    public LoginType[] getLoginType()
    {
        return LoginType.values();
    }

    public LoginType[] getEnvia()
    {
        return new LoginType[]{LoginType.Terminal, LoginType.Vessel_Operator};
    }
}
