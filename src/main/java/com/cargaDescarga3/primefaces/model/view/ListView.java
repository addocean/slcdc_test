package com.cargaDescarga3.primefaces.model.view;

import com.cargaDescarga3.primefaces.enums.EstadoLista;
import com.cargaDescarga3.primefaces.enums.TipoLista;
import com.cargaDescarga3.primefaces.enums.TipoListaConsolidado;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Data
public class ListView
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private TipoLista circuit;
    private TipoListaConsolidado type;
    private String operator;
    private String port;
    private String terminal;
    private Date createdDate;
    private Date updatedDate;
    private Date closingDate;
    private String version;
    private EstadoLista state;

    private String username;
    private String visitNumber;
    private Long userId;

    private Long stopId;
    private Date stopEta;
    private Date stopEtd;
}
