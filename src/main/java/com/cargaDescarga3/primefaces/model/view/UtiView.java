package com.cargaDescarga3.primefaces.model.view;

import com.cargaDescarga3.primefaces.enums.*;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class UtiView
{
    @Id
    private Long id;

    private Long listId;

    private TipoTransporte type;
    private Integer iso6364_code;
    private Afirmacion iso6364_compliance;
    private Float length;
    private Float height;
    private Float width;
    private String plate;
    private EstadoUti state;
    private CircuitoUti circuit;
    private String booking;
    private String bill_of_lading;
    private String port;
    private String nextPort;
    private Float gross_weight;
    private Float verified_weight;
    private Integer extra_height;
    private Integer extra_left_width;
    private Integer extra_right_width;
    private Integer extra_front_height;
    private Integer extra_back_length;
    private String goods_description;
    private String goods_remarks;
    private String instructions;
    private String instructions_remarks;
    private String shipping_company;
    private String shipping_service;
    private Afirmacion soc;
    private String operator;
    private Integer transshipment_call;
}
