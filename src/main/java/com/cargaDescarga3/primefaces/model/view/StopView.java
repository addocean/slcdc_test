package com.cargaDescarga3.primefaces.model.view;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Data
@Entity
public class StopView
{
    @Id
    Long stopId;

    Date stopEta;
    Date stopAta;
    Date stopEtd;
    Date stopAtd;

    Long agentId;
    String agentCif;
    String agentName;

    Long shipId;
    String shipCallSign;
    String shipFlag;
    String shipImo;
    String shipMmsi;
    String shipName;

    Long visitId;
    String visitNextPortCode;
    Date visitEta;
    Date visitEtd;
    Date visitAta;
    Date visitAtd;
    String visitStatusId;
    String visitNumber;

    String locationName;
}