package com.cargaDescarga3.primefaces.model.bean;

import lombok.Data;

@Data
public class MainSummaryBeanData
{
    private int closedLists = 0;
    private int preparingList = 0;
}
