package com.cargaDescarga3.primefaces.model.bean;

import lombok.Data;

import java.util.Date;

@Data
public class StopBeanData
{
    Long stopId;

    Date stopEta;
    Date stopAta;
    Date stopEtd;
    Date stopAtd;

    Long agentId;
    String agentCif;
    String agentName;

    Long shipId;
    String shipCallSign;
    String shipImo;
    String shipMmsi;
    String shipFlag;
    String shipName;

    Long visitId;
    String visitNextPortCode;
    Date visitEta;
    Date visitEtd;
    Date visitAta;
    Date visitAtd;
    String visitStatusId;
    String visitNumber;

    String locationName;

    String stopStringRepresentation;
}