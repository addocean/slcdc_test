package com.cargaDescarga3.primefaces.model.bean;

import com.cargaDescarga3.primefaces.enums.Afirmacion;
import com.cargaDescarga3.primefaces.enums.CircuitoUti;
import com.cargaDescarga3.primefaces.enums.EstadoUti;
import com.cargaDescarga3.primefaces.enums.TipoTransporte;
import lombok.Data;

@Data
public class UtiBeanData
{
    private Long id;
    private Long listId;
    private TipoTransporte type;
    private Integer iso6364_code;
    private Afirmacion iso6364_compliance;
    private Float length;
    private Float height;
    private Float width;
    private String plate;
    private EstadoUti state;
    private CircuitoUti circuit;
    private String booking;
    private String bill_of_lading;
    private String port;
    private String nextPort;
    private Float gross_weight;
    private Float verified_weight;
    private Integer extra_height;
    private Integer extra_left_width;
    private Integer extra_right_width;
    private Integer extra_front_height;
    private Integer extra_back_length;
    private String goods_description;
    private String goods_remarks;
    private String instructions;
    private String instructions_remarks;
    private String shipping_company;
    private String shipping_service;
    private Afirmacion soc;
    private String operator;
    private Integer transshipment_call;
    private Long refeer_id;
    private String type_dangerous_good;
    private String packing_type;
    private String description;
    private String active;
    private String terminal_connected;
    private String ship_connected;
    private String transport_temperature;
    private String max_temperature;
    private String min_temperature;
    private String um_temperature;
    private String diesel_generator;
    private String vent_grilles_state;
    private String vent_grilles_opening;
    private String air_flow;
    private String humidifier;
    private String rel_air_humidity;
    private String atmosphere_control;
    private String oxygen_level;
    private String nitrogen_level;
    private String carbon_dioxide_level;
    private String instructions_refeer;
}
