package com.cargaDescarga3.primefaces.model.bean;

import lombok.Data;

@Data
public class UserBeanData
{
    private String username;

    private Boolean container = false;
}
