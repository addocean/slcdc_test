package com.cargaDescarga3.primefaces.model.bean;

import com.cargaDescarga3.primefaces.enums.EstadoLista;
import com.cargaDescarga3.primefaces.enums.TipoLista;
import com.cargaDescarga3.primefaces.enums.TipoListaConsolidado;
import lombok.Data;

import java.util.Date;

@Data
public class ListBeanData
{
    private Long id;

    private TipoLista circuit;
    private TipoListaConsolidado type;
    private String operator;
    private String port;
    private String terminal;
    private Date createdDate;
    private Date updatedDate;
    private Date closingDate;
    private String version;
    private EstadoLista state;

    private String username;
    private String visitNumber;

    private StopBeanData stopBeanData;
    private UserBeanData userBeanData;

}
