package com.cargaDescarga3.primefaces.model.rest;

import lombok.Data;

@Data
public class LocationDataRest
{
    private Long locationId;
    private String location;
    private String removal;
}
