package com.cargaDescarga3.primefaces.model.rest;

import lombok.Data;

@Data
public class UserDataRest
{
    private String userName;
    private String token;
    private String password;
}
