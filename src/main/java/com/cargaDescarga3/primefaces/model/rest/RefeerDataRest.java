package com.cargaDescarga3.primefaces.model.rest;

import com.cargaDescarga3.primefaces.enums.Afirmacion;
import com.cargaDescarga3.primefaces.enums.Apertura;
import com.cargaDescarga3.primefaces.enums.Temperatura;
import lombok.Data;

@Data
public class RefeerDataRest
{
    private Long refeerId;
    private Afirmacion active;
    private Afirmacion terminal_connected;
    private Afirmacion ship_connected;
    private Float transport_temperature;
    private Float min_temperature;
    private Float max_temperature;
    private Temperatura um_temperature;
    private Afirmacion diesel_generator;
    private Apertura vent_grilles_state;
    private Float vent_grilles_opening;
    private Float air_flow;
    private Afirmacion humidifier;
    private Float rel_air_humidity;
    private Afirmacion atmosphere_control;
    private Float oxygen_level;
    private Float nitrogen_level;
    private Float carbon_dioxide_level;
    private String instructions;

}
