package com.cargaDescarga3.primefaces.model.rest;

import com.cargaDescarga3.primefaces.enums.EstadoLista;
import com.cargaDescarga3.primefaces.enums.TipoLista;
import com.cargaDescarga3.primefaces.enums.TipoListaConsolidado;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class ListRest implements Serializable
{
    private Long list_id;

    private TipoLista circuit;
    private TipoListaConsolidado type;
    private String operator;
    private String port;
    private String terminal;
    private Date created_date;
    private Date updated_date;
    private String updated_user;
    private Date closing_date;
    private String version;
    private EstadoLista state;

    private Long stopId;

    private String username;
}
