package com.cargaDescarga3.primefaces.security;

import com.cargaDescarga3.primefaces.model.data.UserData;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class JWTUtils
{
    public final static String HEADER = "Authorization";
    public final static String PREFIX = "Bearer ";
    public final static String SECRET = "ffe.slcdc.jwt.secret.key";
    public final static String TOKEN_ID = "ADDoceanJWT";

    @Autowired
    private CustomUserDetailsService customUserDetailService;

    public String getTokenForUser(UserData user)
    {
        // List<GrantedAuthority> grantedAuthorities = AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER");
        String[] rolesForUser = customUserDetailService.getRolesForUser(user);
        List<GrantedAuthority> grantedAuthorities = AuthorityUtils.createAuthorityList(rolesForUser);

        String token = Jwts
                .builder()
                .setId(TOKEN_ID)
                .setSubject(user.getUsername() + "_" + user.getUser_id())
                .claim("authorities",
                        grantedAuthorities.stream()
                                .map(GrantedAuthority::getAuthority)
                                .collect(Collectors.toList()))
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 600000))
                .signWith(SignatureAlgorithm.HS512,
                        SECRET.getBytes()).compact();

        return PREFIX + token;
    }
}
