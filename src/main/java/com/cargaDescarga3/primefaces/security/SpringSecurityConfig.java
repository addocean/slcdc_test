package com.cargaDescarga3.primefaces.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter
{
    @Autowired
    private CustomUserDetailsService customUserDetailService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception
    {
/*
        auth.inMemoryAuthentication()
                .withUser("admin").password("{noop}password2").roles("ADMIN", "USER")
                .and()
                .withUser("user").password("{noop}password").roles("USER");
*/

        auth.userDetailsService(customUserDetailService).passwordEncoder(passwordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder()
    {
        return new BCryptPasswordEncoder();
    }

    //.antMatchers("/api/**").permitAll()
    @Override
    protected void configure(HttpSecurity http) throws Exception
    {
        http.csrf().disable()
                .addFilterAfter(new JWTAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class)
                .authorizeRequests().antMatchers(HttpMethod.POST, "/user/").permitAll()
                .and()
                .authorizeRequests().antMatchers("/login*").permitAll()
                .anyRequest().authenticated()
                .and().formLogin().loginPage("/login.xhtml")
                .defaultSuccessUrl("/lists.xhtml", true)
                //.failureUrl("/login.html?error=true")
                .and()
                .logout().logoutSuccessUrl("/login.xhtml");
    }
}
