package com.cargaDescarga3.primefaces.security;

import com.cargaDescarga3.primefaces.model.data.Role;
import com.cargaDescarga3.primefaces.model.data.UserData;
import com.cargaDescarga3.primefaces.services.ILoginDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Set;

@Service
@Transactional
public class CustomUserDetailsService implements UserDetailsService
{
    @Autowired
    private ILoginDataService loginDataManager;

    private Logger logger = LoggerFactory.getLogger(CustomUserDetailsService.class);

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException
    {
        UserData allUser = loginDataManager.getUserByName(userName);

        UserDetails user;
        if (allUser != null)
        {
            user = new User(allUser.getUsername(), allUser.getPassword(), getAuthorities(allUser));
        }
        else
        {
            throw new UsernameNotFoundException("UserData " + userName + " not found");
        }

        return user;
    }

    private Collection<? extends GrantedAuthority> getAuthorities(UserData user)
    {
        String[] rolesString = getRolesForUser(user);
        return AuthorityUtils.createAuthorityList(rolesString);
    }

    public String[] getRolesForUser(UserData user)
    {
        Set<Role> roles = user.getRole_id();
        String[] rolesString = new String[roles.size()];

        int index = 0;
        for (Role r : roles)
        {
            rolesString[index] = r.getRole().toString();
            index++;
        }
        return rolesString;
    }
}
