package com.cargaDescarga3.primefaces.utils;

import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.IOException;

@Named
public class NavigatorUtil
{
    public String headerUrl = "header.xhtml";
    private static final String LISTS_URL = "lists.xhtml";
    private static final String LISTS_DETAILS_URL = "listEdit.xhtml";
    private static final String LOGIN_URL = "login.xhtml";
    private static final String MY_LISTS_URL = "my_lists.xhtml";
    private static final String HEADER_URL = "header.xhtml";

    public static void RedirectToLists() throws IOException
    {
        FacesContext.getCurrentInstance().getExternalContext().redirect(LISTS_URL);
    }

    public static void RedirectToListDetails(Long listId) throws IOException
    {
        String url = LISTS_DETAILS_URL + "?listId=" + listId;
        FacesContext.getCurrentInstance().getExternalContext().redirect(url);
    }

    public static void RedirectToLogin() throws IOException
    {
        FacesContext.getCurrentInstance().getExternalContext().redirect(LOGIN_URL);
    }

    public static void RedirectToMyLists() throws IOException
    {
        FacesContext.getCurrentInstance().getExternalContext().redirect(MY_LISTS_URL);
    }

    public static String GetHeaderURL()
    {
        return HEADER_URL;
    }

    public static String GetListsURL()
    {
        return LISTS_URL;
    }
}
