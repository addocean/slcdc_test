package com.cargaDescarga3.primefaces.converter;

import com.cargaDescarga3.primefaces.model.bean.ListBeanData;
import com.cargaDescarga3.primefaces.model.bean.StopBeanData;
import com.cargaDescarga3.primefaces.model.bean.UserBeanData;
import com.cargaDescarga3.primefaces.model.data.ListData;
import com.cargaDescarga3.primefaces.model.rest.ListRest;
import com.cargaDescarga3.primefaces.model.view.ListView;
import com.cargaDescarga3.primefaces.model.view.StopView;
import com.cargaDescarga3.primefaces.services.IUserDataService;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ListsConverter
{
    private DozerBeanMapper dozerBeanMapper;

    private IUserDataService userDataService;

    @Autowired
    public void setDozerBeanMapper(DozerBeanMapper dozerBeanMapper)
    {
        this.dozerBeanMapper = dozerBeanMapper;
    }

    @Autowired
    public void setUserDataService(IUserDataService userDataService)
    {
        this.userDataService = userDataService;
    }

    public List<ListBeanData> convertListViewListToListBeanDataList(List<ListView> myLists)
    {
        List<ListBeanData> listBean = new ArrayList<>();
        for (ListView listView : myLists)
        {
            listBean.add(convertListViewToListBeanData(listView));
        }
        return listBean;
    }

    public List<ListRest> convertListDataListToListRestList(List<ListData> myLists)
    {
        List<ListRest> listBean = new ArrayList<>();
        for (ListData listView : myLists)
        {
            listBean.add(dozerBeanMapper.map(listView, ListRest.class));
        }
        return listBean;
    }

    public ListData converListRestToListData(ListRest newList)
    {
        return dozerBeanMapper.map(newList, ListData.class);
    }

    public ListBeanData convertListDataToListBeanData(ListData listData, StopView stopView)
    {
        ListBeanData listBeanData = dozerBeanMapper.map(listData, ListBeanData.class);

        StopBeanData stopBeanData = dozerBeanMapper.map(stopView, StopBeanData.class);
        listBeanData.setStopBeanData(stopBeanData);
        return listBeanData;
    }

    public ListBeanData convertListViewToListBeanData(ListView listView)
    {
        ListBeanData listBeanData = dozerBeanMapper.map(listView, ListBeanData.class);

        StopBeanData stopBeanData = dozerBeanMapper.map(listView, StopBeanData.class);
        listBeanData.setStopBeanData(stopBeanData);

        UserBeanData userBeanData = dozerBeanMapper.map(listView, UserBeanData.class);
        listBeanData.setUserBeanData(userBeanData);

        return listBeanData;
    }

    public ListData convertListBeanDataToListData(ListBeanData listBeanData)
    {
        ListData listData = dozerBeanMapper.map(listBeanData, ListData.class);
        return listData;
    }

    public ListRest convertListViewToListRest(ListView listView)
    {
        ListRest listRest = dozerBeanMapper.map(listView, ListRest.class);
        listRest.setList_id(listView.getId());
        return listRest;
    }
}
