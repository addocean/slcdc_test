package com.cargaDescarga3.primefaces.converter;

import com.cargaDescarga3.primefaces.model.bean.UtiBeanData;
import com.cargaDescarga3.primefaces.model.data.ListData;
import com.cargaDescarga3.primefaces.model.data.Uti;
import com.cargaDescarga3.primefaces.model.view.UtiView;
import com.cargaDescarga3.primefaces.model.rest.UtiDataRest;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UtisConverter
{
    private DozerBeanMapper dozerBeanMapper;

    @Autowired
    public void setDozerBeanMapper(DozerBeanMapper dozerBeanMapper)
    {
        this.dozerBeanMapper = dozerBeanMapper;
    }

    public UtiBeanData convertUtiViewToUtiBean(UtiView uti)
    {
        return dozerBeanMapper.map(uti, UtiBeanData.class);
    }

    public List<UtiDataRest> convertListUtiDataToListUtiDataRest(List<Uti> utis)
    {
        List<UtiDataRest> utisDataRest = new ArrayList<>();
        for (Uti uti : utis)
        {
            utisDataRest.add (convertUtiDataToUtiDataRest(uti));
        }
        return utisDataRest;
    }

    public UtiDataRest convertUtiDataToUtiDataRest(Uti uti)
    {
        return dozerBeanMapper.map(uti, UtiDataRest.class);
    }

    public Uti convertUtiDataRestToUtiData(UtiDataRest newUti)
    {
        return dozerBeanMapper.map(newUti, Uti.class);
    }

    public Uti convertUtiBeanDataToUtiData(UtiBeanData utiBeanData, ListData listData)
    {
        Uti uti = dozerBeanMapper.map(utiBeanData, Uti.class);
        uti.setListId(listData);
        return uti;
    }

    public List<UtiBeanData> convertListUtiDataToListUtiBean(List<UtiView> utis)
    {
        List<UtiBeanData> utisDataRest = new ArrayList<>();
        for (UtiView uti : utis)
        {
            utisDataRest.add (convertUtiViewToUtiBean(uti));
        }
        return utisDataRest;
    }
}
