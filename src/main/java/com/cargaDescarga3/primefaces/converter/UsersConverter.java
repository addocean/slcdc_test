package com.cargaDescarga3.primefaces.converter;

import com.cargaDescarga3.primefaces.enums.LoginType;
import com.cargaDescarga3.primefaces.model.bean.UserBeanData;
import com.cargaDescarga3.primefaces.model.data.Role;
import com.cargaDescarga3.primefaces.model.data.UserData;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UsersConverter
{
    private DozerBeanMapper dozerBeanMapper;

    @Autowired
    public void setDozerBeanMapper(DozerBeanMapper dozerBeanMapper)
    {
        this.dozerBeanMapper = dozerBeanMapper;
    }


    public UserBeanData convertFromUserDataToUserBeanData(UserData loggedUser)
    {
        UserBeanData userBeanData = dozerBeanMapper.map(loggedUser, UserBeanData.class);

        boolean isContainerOperator = false;
        for (Role role : loggedUser.getRole_id())
        {
            isContainerOperator |= role.getRole() == LoginType.Container_Operator;

            if (isContainerOperator)
            {
                break;
            }
        }
        userBeanData.setContainer(isContainerOperator);

        return userBeanData;
    }
}
