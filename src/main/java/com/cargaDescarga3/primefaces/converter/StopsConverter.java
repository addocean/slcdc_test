package com.cargaDescarga3.primefaces.converter;

import com.cargaDescarga3.primefaces.model.bean.StopBeanData;
import com.cargaDescarga3.primefaces.model.view.StopView;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class StopsConverter
{
    private DozerBeanMapper dozerBeanMapper;

    @Autowired
    public void setDozerBeanMapper(DozerBeanMapper dozerBeanMapper)
    {
        this.dozerBeanMapper = dozerBeanMapper;
    }

    public List<StopBeanData> convertListsStop(List<StopView> stopsView)
    {
        List<StopBeanData> listBean = new ArrayList<>();
        for (StopView stopView : stopsView)
        {
            StopBeanData stopBeanData = convertStopBeanFromStopView(stopView);
            stopBeanData.setStopStringRepresentation(stopBeanData.getVisitNumber() + "-" + stopBeanData.getLocationName());
            listBean.add(stopBeanData);
        }
        return listBean;
    }

    public StopBeanData convertStopBeanFromStopView(StopView stopView)
    {
        return dozerBeanMapper.map(stopView, StopBeanData.class);
    }
}
