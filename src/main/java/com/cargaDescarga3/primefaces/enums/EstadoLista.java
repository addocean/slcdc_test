package com.cargaDescarga3.primefaces.enums;

public enum EstadoLista
{
    Pendiente, Enviada, Aceptada, Rechazada, Cancelacion_enviada, Cancelada
}
