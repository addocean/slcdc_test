package com.cargaDescarga3.primefaces.enums;

public enum LoginType
{
    Container_Operator, Vessel_Operator, Terminal, Aduana, TTP, APBA, Rep_Aduanero, Emisor, Receptor
}
