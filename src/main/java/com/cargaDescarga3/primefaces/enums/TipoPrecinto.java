package com.cargaDescarga3.primefaces.enums;

public enum TipoPrecinto
{
    Naviera, Terminal, Cuarentena, Aduana, Veterinario
}
