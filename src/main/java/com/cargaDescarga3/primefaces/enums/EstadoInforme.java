package com.cargaDescarga3.primefaces.enums;

public enum EstadoInforme
{
    Pendiente_enviar, Enviado, Recibido, Cancelacion_enviada, Cancelado
}
